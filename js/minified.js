angular.module('growduino').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('partials/alerts.html',
    "\r" +
    "\n" +
    "<div ng-include=\"'partials/loadingProgress.html'\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<form novalidate class=\"form-inline triggers-form alerts\" ng-class=\"{saving: saving}\" ng-hide=\"loading\" name=\"form\" ng-submit=\"saveAlerts()\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('temp1High')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Air Temperature</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"temp1High\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> °C\r" +
    "\n" +
    "                    <span ng-show=\"form.temp1High.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1High.$valid && !form.temp1High.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp1HighEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighEmailAddress.$valid && !form.temp1HighEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp1HighMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighMessageOn.$valid && !form.temp1HighMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp1HighMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighMessageOff.$valid && !form.temp1HighMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('temp1Low')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Air Temperature</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"temp1Low\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> °C\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1Low.$valid && !form.temp1Low.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp1LowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowEmailAddress.$valid && !form.temp1LowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp1LowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowMessageOn.$valid && !form.temp1LowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp1LowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowMessageOff.$valid && !form.temp1LowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('temp2High')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Water Temperature</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"temp2High\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> °C\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2High.$valid && !form.temp2High.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp2HighEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighEmailAddress.$valid && !form.temp2HighEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp2HighMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighMessageOn.$valid && !form.temp2HighMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp2HighMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighMessageOff.$valid && !form.temp2HighMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('temp2Low')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Water Temperature</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"temp2Low\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> °C\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2Low.$valid && !form.temp2Low.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp2LowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowEmailAddress.$valid && !form.temp2LowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp2LowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowMessageOn.$valid && !form.temp2LowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"temp2LowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowMessageOff.$valid && !form.temp2LowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('humidityHigh')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Relative Humidity</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"humidityHigh\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> %\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityHigh.$valid && !form.humidityHigh.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"humidityHighEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityHighEmailAddress.$valid && !form.humidityHighEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"humidityHighMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityHighMessageOn.$valid && !form.humidityHighMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"humidityHighMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityHighMessageOff.$valid && !form.humidityHighMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('humidityLow')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Relative Humidity</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"humidityLow\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> %\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLow.$valid && !form.humidityLow.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"humidityLowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowEmailAddress.$valid && !form.humidityLowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"humidityLowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowMessageOn.$valid && !form.humidityLowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"humidityLowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowMessageOff.$valid && !form.humidityLowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('usndHigh')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When sensor distance from <b>Water Level</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"usndHigh\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\" ng-required=\"alert.active\"> cm\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHigh.$valid && !form.usndHigh.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"usndHighEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighEmailAddress.$valid && !form.usndHighEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"usndHighMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighMessageOn.$valid && !form.usndHighMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"usndHighMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighMessageOff.$valid && !form.usndHighMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "   <div ng-controller=\"AlertController\" ng-init=\"init('usndLow')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When sensor distance from <b>Water Level</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"usndLow\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\" ng-required=\"alert.active\"> cm\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndLow.$valid && !form.usndLow.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"usndLowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndLowEmailAddress.$valid && !form.usndLowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"usndLowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndLowMessageOn.$valid && !form.usndLowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"usndLowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndLowMessageOff.$valid && !form.usndLowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('light2HighTimer')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When  <b>Lighting Indoor</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"light2HighTimer\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2HighTimer.$valid && !form.light2HighTimer.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                and time is from\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"light2HighTimerTimeFrom\" ng-model=\"alert.trigger.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2HighTimerTimeFrom.$valid && !form.light2HighTimerTimeFrom.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"light2HighTimerTimeTo\" ng-model=\"alert.trigger.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">...\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2HighTimerTimeTo.$valid && !form.light2HighTimerTimeTo.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                ...send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"light2HighTimerEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2HighTimerEmailAddress.$valid && !form.light2HighTimerEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"light2HighTimerMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2HighTimerMessageOn.$valid && !form.light2HighTimerMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"light2HighTimerMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2HighTimerMessageOff.$valid && !form.light2HighTimerMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('light2LowTimer')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When  <b>Lighting Indoor</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"light2LowTimer\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2LowTimer.$valid && !form.light2LowTimer.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                and time is from \r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"light2LowTimerTimeFrom\" ng-model=\"alert.trigger.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2LowTimerTimeFrom.$valid && !form.light2LowTimerTimeFrom.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"light2LowTimerTimeTo\" ng-model=\"alert.trigger.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">...\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2LowTimerTimeTo.$valid && !form.light2LowTimerTimeTo.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                ...send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"light2LowTimerEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2LowTimerEmailAddress.$valid && !form.light2LowTimerEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"light2LowTimerMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2LowTimerMessageOn.$valid && !form.light2LowTimerMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"light2LowTimerMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light2LowTimerMessageOff.$valid && !form.light2LowTimerMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('eCHigh')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>EC</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"eCHigh\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d(\\.\\d{1,2})?$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCHigh.$valid && !form.eCHigh.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max one digit, optional decimal point(not comma), then max two decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"2.3\" or \"2.32\" . Incorrect: \"2,32\" or \"23.2\" or \"3.232\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"eCHighEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCHighEmailAddress.$valid && !form.eCHighEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"eCHighMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCHighMessageOn.$valid && !form.eCHighMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"eCHighMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCHighMessageOff.$valid && !form.eCHighMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('eCLow')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>EC</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"eCLow\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d(\\.\\d{1,2})?$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCLow.$valid && !form.eCLow.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max one digit, optional decimal point(not comma), then max two decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"2.3\" or \"2.32\" . Incorrect: \"2,32\" or \"23.2\" or \"3.232\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"eCLowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCLowEmailAddress.$valid && !form.eCLowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"eCLowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCLowMessageOn.$valid && !form.eCLowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"eCLowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.eCLowMessageOff.$valid && !form.eCLowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('pHHigh')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>PH</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"pHHigh\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d(\\.\\d{1,2})?$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHHigh.$valid && !form.pHHigh.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max one digit, optional decimal point(not comma), then max two decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"2.3\" or \"2.32\" . Incorrect: \"2,32\" or \"23.2\" or \"3.232\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"pHHighEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHHighEmailAddress.$valid && !form.pHHighEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"pHHighMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHHighMessageOn.$valid && !form.pHHighMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"pHHighMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHHighMessageOff.$valid && !form.pHHighMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('pHLow')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>PH</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"pHLow\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d(\\.\\d{1,2})?$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHLow.$valid && !form.pHLow.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max one digit, optional decimal point(not comma), then max two decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"2.3\" or \"2.32\" . Incorrect: \"2,32\" or \"23.2\" or \"3.232\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"pHLowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHLowEmailAddress.$valid && !form.pHLowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"pHLowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHLowMessageOn.$valid && !form.pHLowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"pHLowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.pHLowMessageOff.$valid && !form.pHLowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "   <div ng-controller=\"AlertController\" ng-init=\"init('cO2HighTimer')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When  <b>CO2 PPM</b> is over\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"cO2HighTimer\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimer.$valid && !form.cO2HighTimer.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                and time is from\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"cO2HighTimerTimeFrom\" ng-model=\"alert.trigger.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimerTimeFrom.$valid && !form.cO2HighTimerTimeFrom.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"cO2HighTimerTimeTo\" ng-model=\"alert.trigger.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">...\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimerTimeTo.$valid && !form.cO2HighTimerTimeTo.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                ...send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"cO2HighTimerEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimerEmailAddress.$valid && !form.cO2HighTimerEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"cO2HighTimerMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimerMessageOn.$valid && !form.cO2HighTimerMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"cO2HighTimerMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimerMessageOff.$valid && !form.cO2HighTimerMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('cO2LowTimer')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When  <b>CO2 PPM</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"cO2LowTimer\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer.$valid && !form.cO2LowTimer.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                and time is from\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"cO2LowTimerTimeFrom\" ng-model=\"alert.trigger.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimerTimeFrom.$valid && !form.cO2LowTimerTimeFrom.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"cO2LowTimerTimeTo\" ng-model=\"alert.trigger.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"alert.active\">...\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimerTimeTo.$valid && !form.cO2LowTimerTimeTo.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                ...send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"cO2LowTimerEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimerEmailAddress.$valid && !form.cO2LowTimerEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"cO2LowTimerMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimerMessageOn.$valid && !form.cO2LowTimerMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"cO2LowTimerMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimerMessageOff.$valid && !form.cO2LowTimerMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('batteryLow')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>Battery capacity</b> is below\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"batteryLow\" ng-model=\"alert.trigger.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\" ng-required=\"alert.active\"> %\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.batteryLow.$valid && !form.batteryLow.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"batteryLowEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.batteryLowEmailAddress.$valid && !form.batteryLowEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"batteryLowMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.batteryLowMessageOn.$valid && !form.batteryLowMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"batteryLowMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.batteryLowMessageOff.$valid && !form.batteryLowMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-controller=\"AlertController\" ng-init=\"init('powerDown')\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <fieldset class=\"form-group\" ng-disabled=\"!alert.active\">\r" +
    "\n" +
    "                When <b>power is down</b> send notification to\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"powerDownEmailAddress\" class=\"form-control alert-target\" ng-model=\"alert.target\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.powerDownEmailAddress.$valid && !form.powerDownEmailAddress.$error.required\">\r" +
    "\n" +
    "                    Please enter valid email address.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <br>\r" +
    "\n" +
    "                On message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"powerDownMessageOn\" class=\"form-control alert-message\" ng-model=\"alert.on_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.powerDownMessageOn.$valid && !form.powerDownMessageOn.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                Off message:\r" +
    "\n" +
    "                <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                    <input type=\"text\" name=\"powerDownMessageOff\" class=\"form-control alert-message\" ng-model=\"alert.off_message\" ng-pattern=\"/^['!._-\\w ]{1,32}$/\" ng-required=\"alert.active\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.powerDownMessageOff.$valid && !form.powerDownMessageOff.$error.required\">\r" +
    "\n" +
    "                    Only numbers, letters and '!.-_ allowed, max 32 characters.\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "                <span ng-include=\"'partials/alerts/toggleButton.html'\"></span>\r" +
    "\n" +
    "            </fieldset>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <hr>    \r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-1 col-sm-11 form-group\">\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\" ng-hide=\"saving || !form.$valid\">Save</button>\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-disabled\" ng-hide=\"form.$valid\">Save</button>\r" +
    "\n" +
    "            <button type=\"button\" class=\"btn btn-disabled\" ng-hide=\"!saving\">Saving...</button>\r" +
    "\n" +
    "            <span class=\"text-success\" ng-show=\"saveSuccess\">Successfully saved.</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</form>"
  );


  $templateCache.put('partials/alerts/toggleButton.html',
    "<a ng-click=\"toggle()\">\r" +
    "\n" +
    "    <span class=\"glyphicon\" ng-class=\"{'glyphicon-ban-circle': alert.active, 'glyphicon-ok-circle': !alert.active}\"></span>\r" +
    "\n" +
    "</a>"
  );


  $templateCache.put('partials/calibration.html',
    "<div ng-include=\"'partials/loadingProgress.html'\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<form novalidate class=\"form-horizontal calibration-display-form\" name=\"calibration_form\">\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label-left\"></label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Sensor reading&nbsp1</label>\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Real value&nbsp1</label>\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Sensor reading&nbsp2</label>\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Real value&nbsp2</label>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-primary invisible\" ng-show=\"!calibrating && !saving\">Get raw data</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div ng-repeat=\"calibSensor in twoPointCalibrationData\">\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <label class=\"col-sm-2 control-label-left\">{{calibSensor.sensorId.name}}</label>\r" +
    "\n" +
    "            <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "                <input class=\"form-control\" name=\"reading1\" type=\"text\" value=\"{{calibSensor.records[0].reading}}\" disabled>\r" +
    "\n" +
    "                <input class=\"form-control\" name=\"realVal1\" type=\"text\" value=\"{{calibSensor.records[0].realVal}}\" disabled>\r" +
    "\n" +
    "                <input class=\"form-control\" name=\"reading2\" type=\"text\" value=\"{{calibSensor.records[1].reading}}\" disabled>\r" +
    "\n" +
    "                <input class=\"form-control\" name=\"realVal2\" type=\"text\" value=\"{{calibSensor.records[1].realVal}}\" disabled>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <button type=\"button\" class=\"btn btn-primary\" ng-show=\"!calibrating && !saving\" ng-click=\"open_popup_window(calibSensor.sensorId)\">Calibrate</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label-left\"></label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Sensor reading&nbsp1</label>\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Real value&nbsp1</label>\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Sensor reading&nbsp2</label>\r" +
    "\n" +
    "            <label class=\"form-label control-label-top\">Real value&nbsp2</label>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-primary invisible\" ng-show=\"!calibrating && !saving\">Get raw data</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<form novalidate class=\"form-horizontal\" ng-class=\"{saving: saving}\" ng-hide=\"loading\" name=\"form\" ng-submit=\"save()\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">EC 1.278 @20&#176C</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <input name=\"ec_low_ion\" type=\"text\" ng-model=\"config.ec_low_ion\" ng-pattern=\"/^((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])))$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-primary\" ng-show=\"!calibrating && !saving\" ng-click=\"getRawData('ec_low_ion', 'EC')\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"(calibrating && !calibratingArray['ec_low_ion']) || saving\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"calibratingArray['ec_low_ion']\">Getting raw data...</button>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"form.ec_low_ion.$valid && needsSavingArray['ec_low_ion']\">\r" +
    "\n" +
    "          Save settings to set calibration values active!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ec_low_ion.$valid\">\r" +
    "\n" +
    "         Only positive number allowed, no decimals, max 3 digits.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"lastCalibrationFailedArray['ec_low_ion']\">\r" +
    "\n" +
    "         Last calibration attempt of this field failed!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">EC 4.523 @20&#176C</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <input name=\"ec_high_ion\" type=\"text\" ng-model=\"config.ec_high_ion\" ng-pattern=\"/^((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])))$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-primary\" ng-show=\"!calibrating && !saving\" ng-click=\"getRawData('ec_high_ion', 'EC')\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"(calibrating && !calibratingArray['ec_high_ion']) || saving\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"calibratingArray['ec_high_ion']\">Getting raw data...</button>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"form.ec_high_ion.$valid && needsSavingArray['ec_high_ion']\">\r" +
    "\n" +
    "          Save settings to set calibration values active!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ec_high_ion.$valid\">\r" +
    "\n" +
    "         Only positive number allowed, no decimals, max 3 digits.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"lastCalibrationFailedArray['ec_high_ion']\">\r" +
    "\n" +
    "         Last calibration attempt of this field failed!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">EC Offset</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <input name=\"ec_offset\" type=\"text\" ng-model=\"config.ec_offset\" ng-pattern=\"/^-?((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|0))$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ec_offset.$valid\">\r" +
    "\n" +
    "         Positive or negative number or zero allowed, no decimals, max 3 digits.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">pH 4.00 @20&#176C</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <input name=\"ph_4\" type=\"text\" ng-model=\"config.ph_4\" ng-pattern=\"/^((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])))$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-primary\" ng-show=\"!calibrating && !saving\" ng-click=\"getRawData('ph_4', 'pH')\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"(calibrating && !calibratingArray['ph_4']) || saving\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"calibratingArray['ph_4']\">Getting raw data...</button>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"form.ph_4.$valid && needsSavingArray['ph_4']\">\r" +
    "\n" +
    "          Save settings to set calibration values active!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ph_4.$valid\">\r" +
    "\n" +
    "         Only positive number allowed, no decimals, max 3 digits.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"lastCalibrationFailedArray['ph_4']\">\r" +
    "\n" +
    "         Last calibration attempt of this field failed!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">pH 7.03 @20&#176C</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10 calibration-control\">\r" +
    "\n" +
    "            <input name=\"ph_7\" type=\"text\" ng-model=\"config.ph_7\" ng-pattern=\"/^((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])))$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-primary\" ng-show=\"!calibrating && !saving\" ng-click=\"getRawData('ph_7', 'pH')\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"(calibrating && !calibratingArray['ph_7']) || saving\">Get raw data</button>\r" +
    "\n" +
    "        <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"calibratingArray['ph_7']\">Getting raw data...</button>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"form.ph_7.$valid && needsSavingArray['ph_7']\">\r" +
    "\n" +
    "          Save settings to set calibration values active!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ph_7.$valid\">\r" +
    "\n" +
    "         Only positive number allowed, no decimals, max 3 digits.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"lastCalibrationFailedArray['ph_7']\">\r" +
    "\n" +
    "         Last calibration attempt of this field failed!\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-2 col-sm-10\">\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\" ng-show=\"!saving && !calibrating\">Save</button>\r" +
    "\n" +
    "            <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"calibrating\">Save</button>\r" +
    "\n" +
    "            <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"saving\">Saving...</button>\r" +
    "\n" +
    "            <span class=\"text-success\" ng-show=\"saveSuccess\">Successfully saved.</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</form>\r" +
    "\n" +
    "<div id=\"get_calibration_popup_window\" class=\"modal-window\" ng-show=\"show_calibration_popup_window\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <span class=\"close\" ng-click=\"close_popup_window()\">&times;</span>\r" +
    "\n" +
    "        <form class=\"form-horizontal\" name=\"calibration_popup>\">\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <div name=\"label1Row\">\r" +
    "\n" +
    "                    <div class=\"col-sm-2\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-5\">\r" +
    "\n" +
    "                        <label class=\"control-label\">Calibration pair 1</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-4\">\r" +
    "\n" +
    "                        <label class=\"control-label\">Calibration pair 2</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <div name=\"buttonsRow\">\r" +
    "\n" +
    "                    <div class=\"col-sm-2\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-5\">\r" +
    "\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"getRawData(popup_data.sensorId.name, popup_data.sensorId.name, true, 0, popup_data.sensorId.divisor)\">Get raw data</button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-4\">\r" +
    "\n" +
    "                        <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"getRawData(popup_data.sensorId.name, popup_data.sensorId.name, true, 1, popup_data.sensorId.divisor)\">Get raw data</button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <div name=\"fields1Row\">\r" +
    "\n" +
    "                    <label class=\"col-sm-1 control-label\"></label>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <label class=\"control-label\">Sensor data</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <input class=\"form-control\" value=\"{{popup_data.records[0].reading}}\" disabled>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <label class=\"col-sm-1 control-label\"></label>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <label class=\"control-label\">Sensor data</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <input class=\"form-control\" value=\"{{popup_data.records[1].reading}}\" disabled>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <div name=\"fields2Row\">\r" +
    "\n" +
    "                    <label class=\"col-sm-1 control-label\"></label>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <label class=\"control-label\">Real value</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <input class=\"form-control\" ng-model=\"popup_data.records[0].realVal\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <label class=\"col-sm-1 control-label\"></label>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <label class=\"control-label\">Real value</label>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"col-sm-2 calibration-popup-control\">\r" +
    "\n" +
    "                        <input class=\"form-control\" ng-model=\"popup_data.records[1].realVal\">\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"form-group\">\r" +
    "\n" +
    "                <div class=\"col-sm-4\">\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-sm-2\">\r" +
    "\n" +
    "                    <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"sendCalibrationData()\">Save</button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"col-sm-4\">\r" +
    "\n" +
    "                    <button class=\"btn btn-primary\" ng-click=\"close_popup_window()\">Cancel</button>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </form>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/clientUpload.html',
    "..."
  );


  $templateCache.put('partials/growroom.html',
    "<html>\r" +
    "\n" +
    "<head>\r" +
    "\n" +
    "    <style>\r" +
    "\n" +
    "        a{text-decoration:none}\r" +
    "\n" +
    "    </style>\r" +
    "\n" +
    "    <meta http-equiv=\"Content-Language\" content=\"en-us\">\r" +
    "\n" +
    "    <title>Growroom WebCam</title>\r" +
    "\n" +
    "</head>\r" +
    "\n" +
    "<body bgcolor=\"#000000\" vlink=\"#FFFFFF\" alink=\"#FFFFFF\">\r" +
    "\n" +
    "<p align=\"center\"><b><font color=\"#FFFFFF\" face=\"Arial\">\r" +
    "\n" +
    "    <br>\r" +
    "\n" +
    "    <br>\r" +
    "\n" +
    "    <font size=\"1\">Enjoy a nice view :-)</font><br>\r" +
    "\n" +
    "    </font> </b>\r" +
    "\n" +
    "    <iframe src=\"../webcam\" align=\"center\" width=\"640\" height=\"480\" scrolling=\"no\" frameborder=\"no\" marginheight=\"0px\" iframe>\r" +
    "\n" +
    "    \r" +
    "\n" +
    "    \r" +
    "\n" +
    "</iframe></p>\r" +
    "\n" +
    "</body>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "</html>"
  );


  $templateCache.put('partials/loadingProgress.html',
    "<div ng-show=\"loading\">\r" +
    "\n" +
    "    <i class=\"progress-title\">{{loadingMessage}} <span ng-show=\"stepCount\">{{loadingStep}}/{{stepCount}}</span></i>\r" +
    "\n" +
    "    <div class=\"progress progress-striped\" x>\r" +
    "\n" +
    "      <div class=\"progress-bar progress-bar-info\" role=\"progressbar\" aria-valuenow=\"{{loadingStep}}\" aria-valuemin=\"0\" aria-valuemax=\"{{stepCount}}\" style=\"width: {{loadingPercent}}%\">\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/maintenance.html',
    "<a href=\"\" id=\"a\" ng-hide=\"true\"></a>\r" +
    "\n" +
    "<div ng-include=\"'partials/loadingProgress.html'\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-hide=\"loading\" class=\"maintenance-form\">\r" +
    "\n" +
    "    <form name=\"form1\" ng-submit=\"saveClientJSO()\">\r" +
    "\n" +
    "        <div ng-controller=\"MaintenanceController\" ng-init=\"initSaveClientJSO()\" class=\"form-group\">\r" +
    "\n" +
    "            <h2>Backup settings</h2>\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\">Save settings to disk</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form name=\"form3\" ng-submit=\"loadConfig()\">\r" +
    "\n" +
    "        <button class=\"btn btn-primary\">Upload settings from disk</button>\r" +
    "\n" +
    "        <input type=\"file\" ng-model=\"formData.file\" id=\"settingsFileSelector\" ng-hide=\"true\" onchange=\"angular.element(this).scope().loadClientJSO(this)\">\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form name=\"form6\" ng-submit=\"saveCalibJSO()\">\r" +
    "\n" +
    "        <div ng-controller=\"MaintenanceController\" ng-init=\"initSaveCalibJSO()\" class=\"form-group\">\r" +
    "\n" +
    "            <h2>Backup calibration</h2>\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\">Save calibration to disk</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form name=\"form7\" ng-submit=\"loadCalib()\">\r" +
    "\n" +
    "        <button class=\"btn btn-primary\">Upload calibration from disk</button>\r" +
    "\n" +
    "        <input type=\"file\" ng-model=\"formData.file\" id=\"calibrationFileSelector\" ng-hide=\"true\" onchange=\"angular.element(this).scope().loadCalibJSO(this)\">\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form name=\"form2\" ng-submit=\"backupTriggers()\">\r" +
    "\n" +
    "        <div ng-controller=\"MaintenanceController\" ng-init=\"initBackupTriggers()\" class=\"form-group\">\r" +
    "\n" +
    "            <h2>Backup Outputs&Alerts</h2>\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\">Save Outputs&Alerts</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form name=\"form4\" ng-submit=\"loadAlertsTriggers()\">\r" +
    "\n" +
    "        <button class=\"btn btn-primary\">Upload Outputs&Alerts</button>\r" +
    "\n" +
    "        <input type=\"file\" ng-model=\"formData.file\" id=\"outputsAlertsFileSelector\" ng-hide=\"true\" onchange=\"angular.element(this).scope().loadTriggers(this)\">\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form name=\"form5\" ng-submit=\"deleteAlertsTriggers()\">\r" +
    "\n" +
    "        <h2>Delete Outputs&Alerts</h2>\r" +
    "\n" +
    "        <button class=\"btn btn-primary\">Delete Ouputs&Alerts</button>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "    <form name=\"PasswordChange\" action=\"/change_password\">\r" +
    "\n" +
    "        <h2>Change password</h2>\r" +
    "\n" +
    "        <button class=\"btn btn-primary\">Change password</button>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/sensors.html',
    "<form class=\"chart-controls form-inline\" role=\"form\">\r" +
    "\n" +
    "    <div class=\"btn-group\">\r" +
    "\n" +
    "        <button type=\"button\" ng-class=\"{btn: true, 'btn-info': true, active: (zoom == 'M')}\" ng-click=\"changeZoom('M')\">Month</button>\r" +
    "\n" +
    "        <button type=\"button\" ng-class=\"{btn: true, 'btn-info': true, active: (zoom == 'D')}\" ng-click=\"changeZoom('D')\">Day</button>\r" +
    "\n" +
    "        <button type=\"button\" ng-class=\"{btn: true, 'btn-info': true, active: (zoom == 'H')}\" ng-click=\"changeZoom('H')\">Hour</button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"btn-group\">\r" +
    "\n" +
    "        <button title=\"Select date\" type=\"button\" class=\"btn btn-info\" id=\"picker-date\"><span class=\"glyphicon glyphicon-calendar\"></span></button>\r" +
    "\n" +
    "        <button title=\"Show recent data\" type=\"button\" class=\"btn btn-info\" ng-click=\"showRecent()\"><span class=\"glyphicon glyphicon-home\"></span></button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"btn-group\">\r" +
    "\n" +
    "        <button title=\"Previous\" type=\"button\" class=\"btn btn-info\" ng-click=\"shiftDateUnit(-1)\"><span class=\"glyphicon glyphicon-backward\"></span></button>\r" +
    "\n" +
    "        <button title=\"Next\" type=\"button\" class=\"btn btn-info\" ng-click=\"shiftDateUnit(1)\" ng-disabled=\"forwardDisallowed\"><span class=\"glyphicon glyphicon-forward\"></span></button>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <span class=\"selected-date\">{{ formattedDate }}</span>\r" +
    "\n" +
    "</form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div id=\"charts\"></div>"
  );


  $templateCache.put('partials/settings.html',
    "<form novalidate class=\"form-horizontal\" ng-class=\"{loading: loading}\" name=\"form\" ng-submit=\"save()\">\r" +
    "\n" +
    "    <div class=\"form-group\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-2 col-sm-10\">\r" +
    "\n" +
    "            <div class=\"checkbox\">\r" +
    "\n" +
    "                <label><input type=\"checkbox\" ng-model=\"config.use_dhcp\"> DHCP</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-2 col-sm-10\">\r" +
    "\n" +
    "            <div class=\"checkbox\">\r" +
    "\n" +
    "                <label><input type=\"checkbox\" ng-model=\"config.enforce_ssl\"> HTTPS</label>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">MAC address</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"mac\" type=\"text\" ng-model=\"config.mac\" ng-pattern=\"/^([0-9a-f]{2}[:]){5}([0-9a-f]{2})$/\" class=\"form-control\" disabled>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.mac.$valid\">\r" +
    "\n" +
    "         Please enter valid MAC address. Small letters only, separated by : . Example: de:ad:be:ef:55:44 .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">IP address</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"ip\" type=\"text\" ng-model=\"config.ip\" ng-pattern=\"/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ip.$valid\">\r" +
    "\n" +
    "         Please enter valid IP address. From 0.0.0.0 to 255.255.255.255.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">Netmask</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"netmask\" type=\"text\" ng-model=\"config.netmask\" ng-pattern=\"/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.netmask.$valid\">\r" +
    "\n" +
    "         Please enter valid IP address mask. From 0.0.0.0 to 255.255.255.255.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">Gateway</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"gateway\" type=\"text\" ng-model=\"config.gateway\" ng-pattern=\"/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.gateway.$valid\">\r" +
    "\n" +
    "         Please enter valid IP address. From 0.0.0.0 to 255.255.255.255.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <br>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">NTP server</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"ntp\" type=\"text\" ng-model=\"config.ntp\" ng-pattern=\"/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.ntp.$valid\">\r" +
    "\n" +
    "         Please enter valid IP address. From 0.0.0.0 to 255.255.255.255.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">Time zone</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            \r" +
    "\n" +
    "            <select name=\"time_zone\" ng-model=\"config.time_zone\" class=\"form-control\">\r" +
    "\n" +
    "                <option value=\"-12\">GMT -12:00</option>\r" +
    "\n" +
    "                <option value=\"-11\">GMT -11:00</option>\r" +
    "\n" +
    "                <option value=\"-10\">GMT -10:00</option>\r" +
    "\n" +
    "                <option value=\"-9\">GMT -9:00</option>\r" +
    "\n" +
    "                <option value=\"-8\">GMT -8:00</option>\r" +
    "\n" +
    "                <option value=\"-7\">GMT -7:00</option>\r" +
    "\n" +
    "                <option value=\"-6\">GMT -6:00</option>\r" +
    "\n" +
    "                <option value=\"-5\">GMT -5:00</option>\r" +
    "\n" +
    "                <option value=\"-4\">GMT -4:00</option>\r" +
    "\n" +
    "                <option value=\"-3\">GMT -3:00</option>\r" +
    "\n" +
    "                <option value=\"-2\">GMT -2:00</option>\r" +
    "\n" +
    "                <option value=\"-1\">GMT -1:00</option>\r" +
    "\n" +
    "                <option value=\"0\">GMT</option>\r" +
    "\n" +
    "                <option value=\"1\">GMT +1:00</option>\r" +
    "\n" +
    "                <option value=\"2\">GMT +2:00</option>\r" +
    "\n" +
    "                <option value=\"3\">GMT +3:00</option>\r" +
    "\n" +
    "                <option value=\"4\">GMT +4:00</option>\r" +
    "\n" +
    "                <option value=\"5\">GMT +5:00</option>\r" +
    "\n" +
    "                <option value=\"6\">GMT +6:00</option>\r" +
    "\n" +
    "                <option value=\"7\">GMT +7:00</option>\r" +
    "\n" +
    "                <option value=\"8\">GMT +8:00</option>\r" +
    "\n" +
    "                <option value=\"9\">GMT +9:00</option>\r" +
    "\n" +
    "                <option value=\"10\">GMT +10:00</option>\r" +
    "\n" +
    "                <option value=\"11\">GMT +11:00</option>\r" +
    "\n" +
    "                <option value=\"12\">GMT +12:00</option>\r" +
    "\n" +
    "            </select>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <br>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">SMTP server</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"smtp\" type=\"text\" ng-model=\"config.smtp\" ng-pattern=\"/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$/\" or ng-pattern=\"/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.smtp.$valid\">\r" +
    "\n" +
    "         Please enter valid SMTP server address.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">SMTP port</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"smtpPort\" type=\"text\" ng-model=\"config.smtp_port\" ng-pattern=\"/^((((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9])|([1-6][0-5][0-5][0-3][0-5])))))$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.smtpPort.$valid\">\r" +
    "\n" +
    "         Please enter valid port number. From 1 to 65535.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">SMTP Security</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <select name=\"smtp_ssl\" ng-model=\"config.smtp_ssl\" class=\"form-control\">\r" +
    "\n" +
    "                <option value=\"0\">NonSecured port 25</option>\r" +
    "\n" +
    "                <option value=\"ssl\">SSL port 465</option>\r" +
    "\n" +
    "                <option value=\"tls\">TLS port 25 or 587</option>\r" +
    "\n" +
    "            </select>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <label class=\"col-sm-2 control-label\">SMTP Username</label>\r" +
    "\n" +
    "            <div class=\"col-sm-10\">\r" +
    "\n" +
    "                <input name=\"smtp_user\" type=\"text\" ng-model=\"config.smtp_user\" class=\"form-control\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.smtp_user.$valid\">\r" +
    "\n" +
    "         Please enter valid SMTP Username.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <label class=\"col-sm-2 control-label\">SMTP password</label>\r" +
    "\n" +
    "            <div class=\"col-sm-10\">\r" +
    "\n" +
    "                <input name=\"smtp_pwd\" type=\"password\" ng-model=\"config.smtp_pwd\" class=\"form-control\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.smtp_pwd.$valid\">\r" +
    "\n" +
    "          Please enter valid SMTP Password.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"form-group\">\r" +
    "\n" +
    "      <div class=\"col-sm-offset-2 col-sm-2\">\r" +
    "\n" +
    "        <div class=\"btn btn-primary\" ng-click=\"test_email_window()\">\r" +
    "\n" +
    "          Send test e-mail\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <br>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">System name</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"systemName\" type=\"text\" ng-model=\"config.sys_name\" ng-pattern=\"/^[a-zA-Z0-9]{0,16}$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.systemName.$valid\">\r" +
    "\n" +
    "         Only numbers and letters allowed, max 16 characters.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">E-mail from</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"mail_from\" type=\"text\" ng-model=\"config.mail_from\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.mail_from.$valid\">\r" +
    "\n" +
    "         Please enter valid email address.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <br>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">Wifi SSID</label>\r" +
    "\n" +
    "        <div class=\"col-sm-6\">\r" +
    "\n" +
    "            <input name=\"wifi_ssid\" type=\"text\" ng-model=\"config.wifi_ssid\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-sm-2\">\r" +
    "\n" +
    "            <div class=\"btn btn-primary\" ng-click=\"scan_for_wifis()\">\r" +
    "\n" +
    "                Scan for wifis\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-sm-2\">\r" +
    "\n" +
    "            <div name=\"wifi_connected\" class=\"btn label-success\" ng-show=\"wifi_connected\">\r" +
    "\n" +
    "                Connected\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div name=\"wifi_not_connected\" class=\"btn label-default\" ng-show=\"!wifi_connected\">\r" +
    "\n" +
    "                Not connected\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <label class=\"col-sm-2 control-label\">Wifi password</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "            <input name=\"wifi_pwd\" type=\"password\" ng-model=\"config.wifi_pwd\" ng-pattern=\"/^[!-~]{0,63}$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.wifi_pwd.$valid\">\r" +
    "\n" +
    "          Please enter valid password.\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"form-group\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-2 col-sm-10\">\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\" ng-hide=\"saving\">Save</button>\r" +
    "\n" +
    "            <button type=\"button\" class=\"btn btn-disabled\" ng-hide=\"!saving\">Saving...</button>\r" +
    "\n" +
    "            <span class=\"text-success\" ng-show=\"saveSuccess\">Successfully saved.</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</form>\r" +
    "\n" +
    "<div id=\"scan_for_wifi_window\" class=\"modal-window\" ng-show=\"show_wifi_window\">\r" +
    "\n" +
    "    <div class=\"modal-content\">\r" +
    "\n" +
    "        <span class=\"close\" ng-click=\"close_wifi_window()\">&times;</span>\r" +
    "\n" +
    "        <table class=\"table modal-table\">\r" +
    "\n" +
    "            <tbody>\r" +
    "\n" +
    "                <tr>\r" +
    "\n" +
    "                    <th>Quality</th>\r" +
    "\n" +
    "                    <th>SSID</th>\r" +
    "\n" +
    "                    <th>Frequency</th>\r" +
    "\n" +
    "                    <th>Channel</th>\r" +
    "\n" +
    "                    <th>Encrypted</th>\r" +
    "\n" +
    "                </tr>\r" +
    "\n" +
    "                <tr ng-repeat-start=\"item in known_wifis\" ng-click=\"select_wifi_in_modal(item, $index)\" class=\"shadable\" ng-class=\"{shaded: $index==selected_index_in_modal}\">\r" +
    "\n" +
    "                    <td>{{item.quality}}</td>\r" +
    "\n" +
    "                    <td>{{item.ssid}}</td>\r" +
    "\n" +
    "                    <td>{{item.frequency}}</td>\r" +
    "\n" +
    "                    <td>{{item.channel}}</td>\r" +
    "\n" +
    "                    <td ng-if=\"item.encrypted\">Yes</td>\r" +
    "\n" +
    "                    <td ng-if=\"! item.encrypted\">No</td>\r" +
    "\n" +
    "                </tr>\r" +
    "\n" +
    "                <tr ng-repeat-end class=\"pwd_input_in_table\" ng-show=\"$index==selected_index_in_modal\">\r" +
    "\n" +
    "                    <td colspan=\"5\">\r" +
    "\n" +
    "                        <form novalidate class=\"form-horizontal\" ng-submit=\"select_wifi_in_controller(item)\">\r" +
    "\n" +
    "                            <div class=\"form-group\">\r" +
    "\n" +
    "                                <div class=\"col-sm-10\" ng-if=\"item.encrypted\">\r" +
    "\n" +
    "                                    <input class=\"form-control\" type=\"password\" ng-model=\"model.entered_password\">\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div class=\"col-sm-10\" ng-if=\"!item.encrypted\">\r" +
    "\n" +
    "                                    <input class=\"form-control\" type=\"password\" ng-model=\"model.entered_password\" disabled>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div>\r" +
    "\n" +
    "                                    <button type=\"submit\" class=\"btn btn-primary\">Connect</button>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </form>\r" +
    "\n" +
    "                    </td>\r" +
    "\n" +
    "                </tr>\r" +
    "\n" +
    "            </tbody>\r" +
    "\n" +
    "        </table>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div id=\"send_test_email_window\" class=\"modal-window\" ng-show=\"show_test_email_window\">\r" +
    "\n" +
    "  <div class=\"modal-content\">\r" +
    "\n" +
    "    <form class=\"form-horizontal\" name=\"test_email_form\" ng-submit=\"send_test_email()\">\r" +
    "\n" +
    "      <div class=\"form-group\">\r" +
    "\n" +
    "        <label class=\"col-sm-2\">e-mail</label>\r" +
    "\n" +
    "        <div class=\"col-sm-10\">\r" +
    "\n" +
    "          <input name=\"test_email\" type=\"text\" ng-model=\"config.test_email\" ng-pattern=\"/^[a-z0-9!#$%&'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/\" class=\"form-control\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <div class=\"form-group\" ng-show=\"test_email_error_msg\">\r" +
    "\n" +
    "        Error sending test email: {{test_email_error_msg}}\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "      <div class=\"form-group\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-2 col-sm-2\">\r" +
    "\n" +
    "          <button type=\"submit\" class=\"btn btn-primary\" ng-show=\"test_email_form.test_email.$valid\">Send</button>\r" +
    "\n" +
    "          <button type=\"button\" class=\"btn btn-disabled\" ng-show=\"!test_email_form.test_email.$valid\">Send</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-sm-2\">\r" +
    "\n" +
    "          <button type=\"button\" class=\"btn btn-primary\" ng-click=\"close_test_email_window()\">Close</button>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "      </div>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "  </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers.html',
    "<div ng-include=\"'partials/loadingProgress.html'\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<form novalidate class=\"form-inline triggers-form\" ng-class=\"{saving: saving}\" ng-hide=\"loading\" name=\"form\" ng-submit=\"saveTriggers()\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row relay\" ng-repeat=\"relay in relays\">\r" +
    "\n" +
    "        <div class=\"col-sm-3 relay-header\">\r" +
    "\n" +
    "            <div class=\"name\">{{relay.name}}</div>\r" +
    "\n" +
    "            <div class=\"number\">relay {{$index}}</div>\r" +
    "\n" +
    "            <div class=\"btn-group btn-group-xs\">\r" +
    "\n" +
    "                <button type=\"button\" class=\"btn btn-default\" ng-class=\"{active: relay.isPermOn()}\" ng-click=\"relay.setPermOn()\">On</button>\r" +
    "\n" +
    "                <button type=\"button\" class=\"btn btn-default\" ng-class=\"{active: relay.isPermOff()}\" ng-click=\"relay.setPermOff()\">Off</button>\r" +
    "\n" +
    "                <button type=\"button\" class=\"btn btn-default\" ng-class=\"{active: relay.isPermAuto()}\" ng-click=\"relay.setPermAuto()\">Auto</button>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"col-sm-9 relay-body\">\r" +
    "\n" +
    "            <div ng-show=\"relay.isPermOn() && relay.permStatusSaveNeeded()\" class=\"alert alert-warning\">\r" +
    "\n" +
    "                <b>Save</b> triggers to turn on permanently!\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"relay.isPermOn() && !relay.permStatusSaveNeeded()\" class=\"alert alert-success\">\r" +
    "\n" +
    "                Relay is permanently turned on.\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"relay.isPermOff() && relay.permStatusSaveNeeded()\" class=\"alert alert-warning\">\r" +
    "\n" +
    "                <b>Save</b> triggers to disable permanently!\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"relay.isPermOff() && !relay.permStatusSaveNeeded()\" class=\"alert alert-info\">\r" +
    "\n" +
    "                Relay is permanently disabled.\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"relay.isPermAuto() && relay.permStatusSaveNeeded()\" class=\"alert alert-warning\">\r" +
    "\n" +
    "                <b>Save</b> triggers to enable!\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"form-group\" ng-show=\"relay.isPermAuto()\">\r" +
    "\n" +
    "                <div ng-if=\"relay.partial\" ng-include=\"'partials/triggers/'+relay.partial\"></div>\r" +
    "\n" +
    "                <div class=\"timer-line\" ng-repeat=\"range in relay.intervals\">\r" +
    "\n" +
    "                    <fieldset ng-disabled=\"!range.active\">\r" +
    "\n" +
    "                        Turn on from\r" +
    "\n" +
    "                        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                            <input required type=\"text\" name=\"{{relay.name}}_Since_{{$index}}\" class=\"form-control\" ng-model=\"range.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "                        <span ng-show=\"form.{{relay.name}}_Since_{{$index}}.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "                        to\r" +
    "\n" +
    "                        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "                            <input required type=\"text\" name=\"{{relay.name}}_Until_{{$index}}\" class=\"form-control\" ng-model=\"range.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "                            <span ng-show=\"form.{{relay.name}}_Until_{{$index}}.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        \r" +
    "\n" +
    "\r" +
    "\n" +
    "                        <a ng-click=\"relay.toggleInterval($index)\">\r" +
    "\n" +
    "                            <span class=\"glyphicon\" ng-class=\"{'glyphicon-remove': range.active, 'glyphicon-arrow-left': !range.active}\"></span>\r" +
    "\n" +
    "                        </a>\r" +
    "\n" +
    "                    </fieldset>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <a ng-click=\"relay.addInterval()\"><span class=\"glyphicon glyphicon-plus\"></span> add interval</a>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"row relay-save\">\r" +
    "\n" +
    "        <div class=\"col-sm-offset-3 col-sm-9 form-group\">\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\" ng-hide=\"saving || !form.$valid\">Save</button>\r" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-disabled\" ng-hide=\"form.$valid\">Save</button>\r" +
    "\n" +
    "            <button type=\"button\" class=\"btn btn-disabled\" ng-hide=\"!saving\">Saving...</button>\r" +
    "\n" +
    "            <span class=\"text-success\" ng-show=\"saveSuccess\">Successfully saved.</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</form>"
  );


  $templateCache.put('partials/triggers/chuta/co2.html',
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\"> °C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighDisallow_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityHighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.humidityHighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityHighDisallow_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'cO2LowTimer')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when time is from\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_Since\" class=\"form-control\" ng-model=\"t.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        to\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_Until\" class=\"form-control\" ng-model=\"t.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        …<br>\r" +
    "\n" +
    "        …and <b>CO2 PPM</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_OnVal\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\">\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_OnVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_OnVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\">\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2EveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.cO2EveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2EveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2ForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.cO2ForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2ForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/dehumidifier.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHigh')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when  <b>Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumHighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.dehumHighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumHighOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumHighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.dehumHighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumHighOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.dehumEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.dehumForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/fan.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1LowDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Air Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowDisallow\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\"> °C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowDisallow.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowDisallow.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"fanDayNightLabel\">\r" +
    "\n" +
    "    <b>Day ventilation</b>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<fieldset class=\"form-group sentinel-line\" ng-disabled=\"relay.dayDisabled()\">\r" +
    "\n" +
    "    When time is from\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanDay_Since\" class=\"form-control\" ng-model=\"relay.day.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanDay_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanDay_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    to\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanDay_Until\" class=\"form-control\" ng-model=\"relay.day.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanDay_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanDay_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    ...\r" +
    "\n" +
    "</fieldset>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Air Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Relative Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1HighTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1LowTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveForTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanEveryMinute_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.fanEveryMinute_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanEveryMinute_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanForMinute_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.fanForMinute_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanForMinute_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<br>\r" +
    "\n" +
    "<div class=\"fanDayNightLabel\">\r" +
    "\n" +
    "    <b>Night ventilation</b>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<fieldset class=\"form-group sentinel-line\" ng-disabled=\"relay.nightDisabled()\">\r" +
    "\n" +
    "    When time is from\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanNight_Since\" class=\"form-control\" ng-model=\"relay.night.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanNight_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanNight_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    to\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanNight_Until\" class=\"form-control\" ng-model=\"relay.night.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanNight_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanNight_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    ...\r" +
    "\n" +
    "</fieldset>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Air Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Relative Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1HighTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1LowTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveForTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanEveryMinute_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.fanEveryMinute_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanEveryMinute_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanForMinute_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.fanForMinute_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanForMinute_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/freetimer.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"timerEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.timerEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.timerEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"timerForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.timerForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.timerForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/heating.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1Low')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when <b>Air Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"heatEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.heatEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.heatEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"heatForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.heatForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.heatForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/humidifier.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityLow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when  <b>Humidity</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityLowOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.humidityLowOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityLowOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.humidityLowOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.humidityEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.humidityForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/light.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'light1LowTimer')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        Turn on when time is from\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_Since\" class=\"form-control\" ng-model=\"t.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        to\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_Until\" class=\"form-control\" ng-model=\"t.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        …<br>\r" +
    "\n" +
    "        …and <b>Outdoor light</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_OnVal\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_OnVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_OnVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp3HighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Bulb temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp3HighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\"> °C\r" +
    "\n" +
    "\t    <span ng-show=\"form.temp3HighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp3HighDisallow_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/refill.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'usndHigh')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Refill when sensor distance from <b>Water level </b>…<br>\r" +
    "\n" +
    "        …is more than\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\">&nbsp;cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\">&nbsp;cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp2High')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-init=\"t = relay.getTrigger('temp2High')\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Refill when <b>Water Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2HighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2HighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2HighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2HighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"refillEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.refillEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.refillEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"refillForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.refillForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.refillForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/toggleButton.html',
    "<a ng-click=\"toggle()\">\r" +
    "\n" +
    "    <span class=\"glyphicon\" ng-class=\"{'glyphicon-ban-circle': t.active, 'glyphicon-ok-circle': !t.active}\"></span>\r" +
    "\n" +
    "</a>"
  );


  $templateCache.put('partials/triggers/chuta/water_heating.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp2Low')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when <b>Water Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2LowOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2LowOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2LowOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2LowOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterHeatEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.waterHeatEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterHeatEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterHeatForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.waterHeatForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterHeatForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/chuta/watering.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.waterEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.waterForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/co2.html',
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\"> °C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighDisallow_OffVal.$valid && !form.temp1HighDisallow_OffVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityHighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.humidityHighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityHighDisallow_OffVal.$valid && !form.humidityHighDisallow_OffVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'cO2LowTimer')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when time is from\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_Since\" class=\"form-control\" ng-model=\"t.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"t.active\">\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_Since.$valid && !form.cO2LowTimer_Since.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        to\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_Until\" class=\"form-control\" ng-model=\"t.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"t.active\">\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_Until.$valid && !form.cO2LowTimer_Until.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        …<br>\r" +
    "\n" +
    "        …and <b>CO2 concentration</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_OnVal\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> PPM\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_OnVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_OnVal.$valid && !form.cO2LowTimer_OnVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> PPM\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_OffVal.$valid && !form.cO2LowTimer_OffVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2EveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.cO2EveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2EveryMinute.$valid && !form.cO2EveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2ForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.cO2ForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2ForMinute.$valid && !form.cO2ForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/dehumidifier.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHigh')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when  <b>Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumHighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.dehumHighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumHighOn.$valid && !form.dehumHighOn.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumHighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.dehumHighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumHighOff.$valid && !form.dehumHighOff.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.dehumEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumEveryMinute.$valid && !form.dehumEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"dehumForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.dehumForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.dehumForMinute.$valid && !form.dehumForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/fan.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1LowDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Air Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowDisallow\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\"> °C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowDisallow.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowDisallow.$valid && !form.temp1LowDisallow.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"fanDayNightLabel\">\r" +
    "\n" +
    "    <b>Day ventilation</b>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<fieldset class=\"form-group sentinel-line\" ng-disabled=\"relay.dayDisabled()\">\r" +
    "\n" +
    "    When time is from\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "          <input type=\"text\" name=\"fanDay_Since\" class=\"form-control\" ng-model=\"relay.day.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"!relay.dayDisabled()\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanDay_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanDay_Since.$valid && !form.fanDay_Since.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    to\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanDay_Until\" class=\"form-control\" ng-model=\"relay.day.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"!relay.dayDisabled()\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanDay_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanDay_Until.$valid && !form.fanDay_Until.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    ...\r" +
    "\n" +
    "</fieldset>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Air Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighTimer_day.$valid && !form.temp1HighTimer_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowTimer_day.$valid && !form.temp1LowTimer_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Relative Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1HighTimer_day.$valid && !form.hum1HighTimer_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1LowTimer_day.$valid && !form.hum1LowTimer_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'cO2HighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>CO2 concentration</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> PPM\r" +
    "\n" +
    "            <span ng-show=\"form.cO2HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimer_day.$valid && !form.cO2HighTimer_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> PPM\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_day.$valid && !form.cO2LowTimer_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveForTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanEveryMinute_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.fanEveryMinute_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanEveryMinute_day.$valid && !form.fanEveryMinute_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanForMinute_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.fanForMinute_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanForMinute_day.$valid && !form.fanForMinute_day.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<br>\r" +
    "\n" +
    "<div class=\"fanDayNightLabel\">\r" +
    "\n" +
    "    <b>Night ventilation</b>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<fieldset class=\"form-group sentinel-line\" ng-disabled=\"relay.nightDisabled()\">\r" +
    "\n" +
    "    When time is from\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanNight_Since\" class=\"form-control\" ng-model=\"relay.night.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"!relay.nightDisabled()\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanNight_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanNight_Since.$valid && !form.fanNight_Since.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    to\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanNight_Until\" class=\"form-control\" ng-model=\"relay.night.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"!relay.nightDisabled()\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanNight_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanNight_Until.$valid && !form.fanNight_Until.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    ...\r" +
    "\n" +
    "</fieldset>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Air Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighTimer_night.$valid && !form.temp1HighTimer_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowTimer_night.$valid && !form.temp1LowTimer_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Relative Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1HighTimer_night.$valid && !form.hum1HighTimer_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1LowTimer_night.$valid && !form.hum1LowTimer_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'cO2HighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>CO2 concentration</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> PPM\r" +
    "\n" +
    "            <span ng-show=\"form.cO2HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2HighTimer_night.$valid && !form.cO2HighTimer_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"cO2LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> PPM\r" +
    "\n" +
    "            <span ng-show=\"form.cO2LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.cO2LowTimer_night.$valid && !form.cO2LowTimer_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveForTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanEveryMinute_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.fanEveryMinute_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanEveryMinute_night.$valid && !form.fanEveryMinute_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanForMinute_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.fanForMinute_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanForMinute_night.$valid && !form.fanForMinute_night.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/fish/fan.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1LowDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Air Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowDisallow\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\"> °C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowDisallow.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowDisallow.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div class=\"fanDayNightLabel\">\r" +
    "\n" +
    "    <b>Day ventilation</b>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<fieldset class=\"form-group sentinel-line\" ng-disabled=\"relay.dayDisabled()\">\r" +
    "\n" +
    "    When time is from\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanDay_Since\" class=\"form-control\" ng-model=\"relay.day.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanDay_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanDay_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    to\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanDay_Until\" class=\"form-control\" ng-model=\"relay.day.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanDay_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanDay_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    ...\r" +
    "\n" +
    "</fieldset>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Air Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Relative Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1HighTimer_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1HighTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1HighTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1LowTimer_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1LowTimer_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1LowTimer_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveForTimer_day')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanEveryMinute_day\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.fanEveryMinute_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanEveryMinute_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanForMinute_day\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.fanForMinute_day.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanForMinute_day.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<br>\r" +
    "\n" +
    "<div class=\"fanDayNightLabel\">\r" +
    "\n" +
    "    <b>Night ventilation</b>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<fieldset class=\"form-group sentinel-line\" ng-disabled=\"relay.nightDisabled()\">\r" +
    "\n" +
    "    When time is from\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanNight_Since\" class=\"form-control\" ng-model=\"relay.night.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanNight_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanNight_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    to\r" +
    "\n" +
    "    <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "        <input type=\"text\" name=\"fanNight_Until\" class=\"form-control\" ng-model=\"relay.night.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "        <span ng-show=\"form.fanNight_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanNight_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "    </span>\r" +
    "\n" +
    "    ...\r" +
    "\n" +
    "</fieldset>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1HighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Air Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1HighTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityHighTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... turn on when <b>Relative Humidity</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1HighTimer_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1HighTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1HighTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"hum1LowTimer_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.hum1LowTimer_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.hum1LowTimer_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveForTimer_night')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        ... enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanEveryMinute_night\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.fanEveryMinute_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanEveryMinute_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"fanForMinute_night\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.fanForMinute_night.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.fanForMinute_night.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/fish/light.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'light1LowTimer')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        Turn on when time is from\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_Since\" class=\"form-control\" ng-model=\"t.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_Since.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        to\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_Until\" class=\"form-control\" ng-model=\"t.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_Until.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        …<br>\r" +
    "\n" +
    "        …and <b>Outdoor light</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_OnVal\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_OnVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_OnVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">\r" +
    "\n" +
    "\t    <span ng-show=\"form.light1LowTimer_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp3HighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Bulb temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp3HighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\"> °C\r" +
    "\n" +
    "\t    <span ng-show=\"form.temp3HighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp3HighDisallow_OffVal.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/fish/refill.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'usndHigh')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Refill when sensor distance from <b>Water level </b>…<br>\r" +
    "\n" +
    "        …is more than\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\">&nbsp;cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\">&nbsp;cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp2High')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-init=\"t = relay.getTrigger('temp2High')\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Refill when <b>Water Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2HighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2HighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighOn.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2HighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^\\d{1,2}(\\.\\d)?$/\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2HighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighOff.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"refillEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.refillEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.refillEveryMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"refillForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.refillForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.refillForMinute.$valid\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/freetimer.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"timerEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.timerEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.timerEveryMinute.$valid && !form.timerEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"timerForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.timerForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.timerForMinute.$valid && !form.timerForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/heating.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp1Low')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when <b>Air Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowOn.$valid && !form.temp1LowOn.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp1LowOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp1LowOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp1LowOff.$valid && !form.temp1LowOff.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"heatEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.heatEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.heatEveryMinute.$valid && !form.heatEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"heatForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.heatForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.heatForMinute.$valid && !form.heatForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/humidifier.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'humidityLow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when  <b>Humidity</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityLowOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.humidityLowOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowOn.$valid && !form.humidityLowOn.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityLowOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;%\r" +
    "\n" +
    "            <span ng-show=\"form.humidityLowOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityLowOff.$valid && !form.humidityLowOff.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.humidityEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityEveryMinute.$valid && !form.humidityEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"humidityForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes.\r" +
    "\n" +
    "            <span ng-show=\"form.humidityForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.humidityForMinute.$valid && !form.humidityForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/light.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'light1LowTimer')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        \r" +
    "\n" +
    "        Turn on when time is from\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_Since\" class=\"form-control\" ng-model=\"t.since\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"t.active\">\r" +
    "\n" +
    "            <span ng-show=\"form.light1LowTimer_Since.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_Since.$valid && !form.light1LowTimer_Since.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        to\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_Until\" class=\"form-control\" ng-model=\"t.until\" ng-pattern=\"/^((([0-1][0-9]):([0-5][0-9])|([2][0-3]):([0-5][0-9])|(24:00)))$/\" ng-required=\"t.active\">\r" +
    "\n" +
    "            <span ng-show=\"form.light1LowTimer_Until.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_Until.$valid && !form.light1LowTimer_Until.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be in hh:mm format from 00:00 to 24:00.\r" +
    "\n" +
    "                <br>Correct: \"02:23\" or \"23:00\" or \"23:23\" . Incorrect: \"23\" or \"02:3\" or \"2300\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        …<br>\r" +
    "\n" +
    "        …and <b>Outdoor light</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_OnVal\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9])$/\" ng-required=\"t.active\">\r" +
    "\n" +
    "            <span ng-show=\"form.light1LowTimer_OnVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_OnVal.$valid && !form.light1LowTimer_OnVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma).\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"2.3\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"light1LowTimer_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9])$/\" ng-required=\"t.active\">\r" +
    "\n" +
    "            <span ng-show=\"form.light1LowTimer_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.light1LowTimer_OffVal.$valid && !form.light1LowTimer_OffVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma).\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"2.3\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp3HighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>Bulb temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp3HighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\"> °C\r" +
    "\n" +
    "            <span ng-show=\"form.temp3HighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp3HighDisallow_OffVal.$valid && !form.temp3HighDisallow_OffVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/refill.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'usndHigh')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Refill when sensor distance from <b>Water level </b>…<br>\r" +
    "\n" +
    "        …is more than\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\" ng-required=\"t.active\">&nbsp;cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighOn.$valid && !form.usndHighOn.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, without decimals..\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\" ng-required=\"t.active\">&nbsp;cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighOff.$valid && !form.usndHighOff.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, no decimals.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "                </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp2High')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-init=\"t = relay.getTrigger('temp2High')\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Refill when <b>Water Temperature</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2HighOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2HighOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighOn.$valid && !form.temp2HighOn.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it open till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2HighOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2HighOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2HighOff.$valid && !form.temp2HighOff.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"refillEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.refillEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.refillEveryMinute.$valid && !form.refillEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"refillForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.refillForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.refillForMinute.$valid && !form.refillForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/toggleButton.html',
    "<a ng-click=\"toggle()\">\r" +
    "\n" +
    "    <span class=\"glyphicon\" ng-class=\"{'glyphicon-ban-circle': t.active, 'glyphicon-ok-circle': !t.active}\"></span>\r" +
    "\n" +
    "</a>"
  );


  $templateCache.put('partials/triggers/water_heating.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'temp2Low')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Turn on when <b>Water Temperature</b> is below\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2LowOn\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2LowOn.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowOn.$valid && !form.temp2LowOn.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        then keep it on till\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"temp2LowOff\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(?:0\\.[1-9]|[1-9]|[1-9]\\.[1-9]|[1-9][0-9]|[1-9][0-9]\\.[1-9])$/\" ng-required=\"t.active\">&nbsp;°C\r" +
    "\n" +
    "            <span ng-show=\"form.temp2LowOff.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.temp2LowOff.$valid && !form.temp2LowOff.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, optional decimal point(not comma), then max one decimal.\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\" or \"23.2\" . Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        is reached.\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterHeatEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.waterHeatEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterHeatEveryMinute.$valid && !form.waterHeatEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterHeatForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.waterHeatForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterHeatForMinute.$valid && !form.waterHeatForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('partials/triggers/watering.html',
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'usndHighDisallow')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Never turn on when <b>sensor distance from water level</b> is over\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"usndHighDisallow_OffVal\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(([1-9])|([1-9][0-9]))$/\" ng-required=\"t.active\"> cm\r" +
    "\n" +
    "            <span ng-show=\"form.usndHighDisallow_OffVal.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "<span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.usndHighDisallow_OffVal.$valid && !form.usndHighDisallow_OffVal.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max two digits, without decimals..\r" +
    "\n" +
    "                <br>Correct: \"3\" or \"23\". Incorrect: \"232\" or \"23,2\" or \"23.25\" .\r" +
    "\n" +
    "            </span>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "<div ng-controller=\"TriggerController\" ng-init=\"init(relay, 'inactiveFor')\">\r" +
    "\n" +
    "    <fieldset class=\"form-group sentinel-line\" ng-disabled=\"!t.active\">\r" +
    "\n" +
    "        Enable every\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterEveryMinute\" class=\"form-control\" ng-model=\"t.on.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes\r" +
    "\n" +
    "            <span ng-show=\"form.waterEveryMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterEveryMinute.$valid && !form.waterEveryMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        of inactivity for\r" +
    "\n" +
    "        <div class=\"form-group\" bs-has-error>\r" +
    "\n" +
    "            <input type=\"text\" name=\"waterForMinute\" class=\"form-control\" ng-model=\"t.off.val\" ng-pattern=\"/^(((([1-9])|([1-9][0-9])|([1-9][0-9][0-9])|([1-9][0-9][0-9][0-9]))))$/\" ng-required=\"t.active\"> minutes...\r" +
    "\n" +
    "            <span ng-show=\"form.waterForMinute.$invalid && showInvalids\" class=\"formError\">Incorrect value</span>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <span style=\"color:#B40404\" class=\"help-block\" ng-show=\"!form.waterForMinute.$valid && !form.waterForMinute.$error.required\">\r" +
    "\n" +
    "                    Invalid! Must be number with max four digits, without decimals.\r" +
    "\n" +
    "                <br>Correct: \"2\" or \"23\" or \"2323\" . Incorrect: \"2,32\" or \"23.2\" or \"232323\" .\r" +
    "\n" +
    "        </span>\r" +
    "\n" +
    "        <span ng-include=\"'partials/triggers/toggleButton.html'\"></span>\r" +
    "\n" +
    "    </fieldset>\r" +
    "\n" +
    "</div>"
  );

}]);

app.factory("utils",function(){var a={};return a.fixNull=function(a){return a.map(function(a){return-999==a?null:a})},a.daysInMonth=function(a,b){return new Date(b,a+1,0).getDate()},a.arrayPad=function(a,b,c){for(var d=b-a.length,e=a.slice();d-->0;)e.push(c);return e},a.timeToMinutes=function(a){if("string"!==$.type(a))return null;var b=a.split(":");return 60*parseInt(b[0],10)+parseInt(b[1],10)},a.minutesToTime=function(a){if(!$.isNumeric(a))return null;var b=parseInt(a/60,10),c=a%60;return(10>b?"0":"")+b+":"+(10>c?"0":"")+c},a.generateId=function(){var a=String.fromCharCode(65+Math.floor(26*Math.random())),b=a+Date.now();return"id_"+b},a.seq=function(a){for(var b=[],c=0;a>c;c++)b.push(c);return b},a.arrayUnique=function(a){return a.reduce(function(a,b){return a.indexOf(b)<0&&a.push(b),a},[])},a.newArray=function(a,b){return Array.apply(null,new Array(a)).map(function(){return b})},a.deepCompare=function(){function a(d,e){var f;if(isNaN(d)&&isNaN(e)&&"number"==typeof d&&"number"==typeof e)return!0;if(d===e)return!0;if("function"==typeof d&&"function"==typeof e||d instanceof Date&&e instanceof Date||d instanceof RegExp&&e instanceof RegExp||d instanceof String&&e instanceof String||d instanceof Number&&e instanceof Number)return d.toString()===e.toString();if(!(d instanceof Object&&e instanceof Object))return!1;if(d.isPrototypeOf(e)||e.isPrototypeOf(d))return!1;if(d.constructor!==e.constructor)return!1;if(d.prototype!==e.prototype)return!1;if(b.indexOf(d)>-1||c.indexOf(e)>-1)return!1;for(f in e){if(e.hasOwnProperty(f)!==d.hasOwnProperty(f))return!1;if(typeof e[f]!=typeof d[f])return!1}for(f in d){if(e.hasOwnProperty(f)!==d.hasOwnProperty(f))return!1;if(typeof e[f]!=typeof d[f])return!1;switch(typeof d[f]){case"object":case"function":if(b.push(d),c.push(e),!a(d[f],e[f]))return!1;b.pop(),c.pop();break;default:if(d[f]!==e[f])return!1}}return!0}var b,c;if(arguments.length<1)return!0;for(var d=1,e=arguments.length;e>d;d++)if(b=[],c=[],!a(arguments[0],arguments[d]))return!1;return!0},a}),app.factory("requests",["$q",function(a){var b=async.queue(function(a,b){var c=a.deferred;a.fn().then(c.resolve,c.reject,c.notify)["finally"](b)},1),c={};return c.push=function(c){var d=a.defer();return b.push({fn:c,deferred:d}),d.promise},c.clear=function(){b.tasks.splice(0,b.tasks.length)},c.adapt=function(a,b){"undefined"==typeof b&&(b=["get","save","post"]);var d={};return b.forEach(function(b){d[b]=function(){var d=arguments;return c.push(function(){return a[b].apply(a,d).$promise})}}),d},c}]),app.factory("BackendConfig",["$resource","requests",function(a,b){return b.adapt(a("/config.jso"))}]),app.factory("CalibrationConfig",["$resource","requests",function(a,b){return{config:b.adapt(a("/calib.jso")),"DHT22-Hum":b.adapt(a("/sensors/rawdata/0.jso")),"DHT22-Temp":b.adapt(a("/sensors/rawdata/1.jso")),"Light-Out":b.adapt(a("/sensors/rawdata/2.jso")),USND:b.adapt(a("/sensors/rawdata/3.jso")),"Temp-Water":b.adapt(a("/sensors/rawdata/4.jso")),"Light-In":b.adapt(a("/sensors/rawdata/5.jso")),"Temp-Bulb":b.adapt(a("/sensors/rawdata/6.jso")),EC:b.adapt(a("/sensors/rawdata/7.jso")),pH:b.adapt(a("/sensors/rawdata/8.jso")),CO2:b.adapt(a("/sensors/rawdata/9.jso"))}}]),app.factory("ClientConfig",["$resource","requests",function(a,b){var c=a("/client.jso"),d=b.adapt(c),e=d.get;return d.get=function(){return e.apply(this,arguments)["catch"](function(){return new c})},d}]),app.factory("sensorResourceFactory",["$resource","requests","$http","utils",function(a,b,c,e){return function(b,f){var g=c.defaults.transformResponse.concat([function(a,b){return["day","h","min"].forEach(function(b){$.isArray(a[b])&&(d=a[b],d=e.fixNull(d),a[b]=f?f(d):d)}),a}]);return a("/sensors/"+b+".jso",{},{get:{method:"GET",transformResponse:g},getMonth:{method:"GET",url:"/DATA/"+b.toUpperCase()+"/:year/:month.JSO",transformResponse:g},getDay:{method:"GET",url:"/DATA/"+b.toUpperCase()+"/:year/:month/:day.JSO",transformResponse:g},getHour:{method:"GET",url:"/DATA/"+b.toUpperCase()+"/:year/:month/:day/:hour.JSO",transformResponse:g}})}}]),app.factory("SensorHistory",["$q","sensorResourceFactory","divisors","settings","requests",function(a,b,c,d,e){function f(a,b,c,d){g.forEach(function(f){e.push(function(){return f.resource[a](b,function(a){c(f.name,a)},function(a){d(f.name)}).$promise})})}var g=[];return d.sensors.forEach(function(a){g.push({name:a.resource,resource:b(a.resource,c.get(a.divisor))})}),{get:function(a,b,c){return f("get",a,b,c)},getMonth:function(a,b,c){return f("getMonth",a,b,c)},getDay:function(a,b,c){return f("getDay",a,b,c)},getHour:function(a,b,c){return f("getHour",a,b,c)}}}]),app.factory("RelayData",["$resource","$http","settings","requests",function(a,b,c,d){var e=b.defaults.transformResponse.concat([function(a,b){var c=[];for(var d in a.state)c.push({when:moment.unix(d),state:a.state[d],show:!0});return c.sort(function(a,b){return b.when.valueOf()-a.when.valueOf()}),a.currentState=0===c.length?0:c[0].state,a.history=c,a}]);return d.adapt(a("/sensors/outputs.jso",{},{get:{method:"GET",transformResponse:e}}))}]),app.factory("OutputsData",["$resource","$http","settings","requests",function(a,b,c,d){var e=b.defaults.transformResponse.concat([function(a,b){var c=[];for(var d in a.state)c.push({when:moment.unix(d),state:a.state[d],show:!0});return c.sort(function(a,b){return b.when.valueOf()-a.when.valueOf()}),a.history=c,a}]);return d.adapt(a("/sensors/output_changes.jso",{},{get:{method:"GET",transformResponse:e}}))}]),app.factory("divisors",function(){var a=function(a,b){return a.map(function(a){return null===a?null:a/parseFloat(b)})};return{get:function(b){return function(c){return a(c,b)}}}}),app.factory("Trigger",["$http","$q","requests","settings","utils",function(a,b,c,d,e){function f(a){for(var b=a.toLowerCase(),c=0;c<l.length;c++){if(l[c][0].toLowerCase()===b)return{pattern:l[c][1],sensor:null};for(var e=0;e<d.sensors.length;e++)if((d.sensors[e].resource+l[c][0]).toLowerCase()===b)return{pattern:l[c][1],sensor:e}}return console.warn("No pattern found for "+a),null}function g(a){if(0>a)return 1;var b=d.sensors[a];return b?b.divisor:(console.warn("Value for undeclared sensor: "+a),10)}function h(a,b){var c=parseInt(a.substring(1),10);return-256>=c?c=Number.NEGATIVE_INFINITY:"T"!==a[0]&&(c/=g(b)),{op:a[0],val:c,important:-1!==a.indexOf("!")}}function i(a,b){var c;return c=a.val===Number.NEGATIVE_INFINITY||null===a.val?-512:"T"===a.op?a.val:parseInt(a.val*g(b),10),a.op+c+(a.important?"!":"")}function j(a){return"**:**"===a?"00:00":"*"===a?null:a}function k(a,b,c){for(var d in b){var e=b[d],f=a[d];if($.isPlainObject(e)){if(!k(f,e,c))return!1}else if("**:**"===e||"*"===e){if(null===f||"undefined"===$.type(f))return!1;if(f===Number.NEGATIVE_INFINITY)return!1;if(c&&""===f)return!1}else if(f!=e)return!1}return!0}var l=[["LowDisallow",{off:{important:!0,op:"<",val:"*"}}],["HighDisallow",{off:{important:!0,op:">",val:"*"}}],["InactiveFor",{since:null,on:{op:"T",val:"*"},off:{op:"T",val:"*"}}],["InactiveForTimer",{since:"**:**",until:"**:**",on:{op:"T",val:"*"},off:{op:"T",val:"*"}}],["High",{since:null,on:{op:">",val:"*"},off:{op:"<",val:"*"}}],["LowNoStopTimer",{since:"**:**",until:"**:**",on:{op:"<",val:"*"},off:{op:"<",val:Number.NEGATIVE_INFINITY}}],["LowTimer",{since:"**:**",until:"**:**",on:{op:"<",val:"*"},off:{op:">",val:"*"}}],["HighTimer",{since:"**:**",until:"**:**",on:{op:">",val:"*"},off:{op:"<",val:"*"}}],["Low",{since:null,on:{op:"<",val:"*"},off:{op:">",val:"*"}}],["ManualOn",{since:"00:00",until:"24:00",on:{op:"<",val:Number.NEGATIVE_INFINITY},off:{op:">",val:Number.NEGATIVE_INFINITY},sensor:null}],["Timer",{since:"**:**",until:"**:**",on:{op:"<",val:Number.NEGATIVE_INFINITY},off:{op:">",val:Number.NEGATIVE_INFINITY},sensor:null}]],m=function(){this.since=null,this.until=null,this.on={op:null,val:null,important:!1},this.off={op:null,val:null,important:!1}};m.prototype.unpack=function(a){this.since=-1===a.t_since?null:e.minutesToTime(a.t_since),this.until=-1===a.t_since?null:e.minutesToTime(a.t_until),this.on=h(a.on_value,a.sensor),this.off=h(a.off_value,a.sensor),this.sensor=-1===a.sensor?null:a.sensor,this.output=a.output,null!==a.index&&(this.index=a.index),this.origin=a},m.prototype.pack=function(a){return this.match(this.triggerClass.split("_")[0],a)?"timer"===this.triggerClass&&"00:00"===this.since&&"00:00"===this.until?null:(this.off.important&&(this.on={val:this.off.val,op:"<"===this.off.op?">":"<",important:!1}),raw={t_since:null===this.since?-1:e.timeToMinutes(this.since),t_until:null===this.since?0:e.timeToMinutes(this.until),off_value:i(this.off,this.sensor),on_value:i(this.on,this.sensor),sensor:null===this.sensor?-1:this.sensor,output:this.output},null!==this.index&&void 0!==this.index&&(raw.index=this.index),raw):null},m.prototype.match=function(a,b){return"string"===$.type(a)&&(a=f(a).pattern),k(this,a,b)},m.prototype.getName=function(){for(var a=0;a<l.length;a++)if(this.match(l[a][1])){var b=(null!==this.sensor?d.sensors[this.sensor].resource:"")+l[a][0];return b[0].toLowerCase()+b.substring(1)}return null},m.prototype.update=function(a){for(var b=Object.keys(a),c=0;c<b.length;c++)this[b[c]]=a[b[c]]},m.prototype.prepareSave=function(a){this.actualPack=this.pack(!0),this.actualPack&&(this.actualPack.active=a,delete this.actualPack.index)},m.prototype.isReleased=function(){return void 0!=this.index&&null==this.actualPack?!0:!1},m.prototype.useSlotIndex=function(a){return void 0==this.index&&null!=this.actualPack?(this.index=a,!0):!1},m.prototype.saveTrigger=function(b){return this.index>-1?(this.origin&&this.origin.index&&delete this.origin.index,e.deepCompare(this.actualPack,this.origin)?b():(console.log("Saving trigger #"+this.index+", named "+this.triggerClass),console.log("Actual:"),console.log(this.actualPack),console.log("Origin:"),console.log(this.origin),this.origin=this.actualPack,a.post("/triggers/"+this.index+".jso",this.actualPack).success(function(){b()})),this.index):(b(),-1)},m.prototype.deleteTrigger=function(b){var c=m.createDisabled(0);c.active=0,delete c.index,a.post("/triggers/"+this.index+".jso",c).success(function(){b()})};var n=d.triggerCount-d.alertLimit;return d.triggerLimit&&(n=Math.min(n,d.triggerLimit)),$.extend(m,{LENGTH:n,createDisabled:function(a){return{t_since:-1,t_until:-1,on_value:"<-256",off_value:">-512",sensor:-1,output:-1,index:a}},loadMany:function(b,d,e){if(0===b.length)return void e();var f;b.forEach(function(b){f=c.push(function(){return a.get("/triggers/"+b+".jso",{cache:!1}).success(function(a){console.log("trigger #"+b+" loaded",a),a.index=b,d(a)})})}),$.isFunction(e)&&f["finally"](e)},loadRaw:function(b,c,d){a.get("/triggers/"+b+".jso",{cache:!1}).success(function(a){c(a),d()})},save:function(b,d){if(!b.length)return void d();var e;b.forEach(function(b){e=c.push(function(){var c=b.index;return a.post("/triggers/"+c+".jso",b)})}),$.isFunction(d)&&e["finally"](d)},create:function(a){var b=f(a),c=b.pattern,d=new m;d.sensor=b.sensor;for(var e in c){var g=c[e];if($.isPlainObject(g))for(var h in g)d[e][h]=j(g[h]);else d[e]=j(g)}return d.triggerClass=a,d.active=!1,d},unpack:function(a){var b=new m;b.unpack(a);var c=b.getName();return c?(b.triggerClass=c,b):null}}),m}]),app.factory("Relay",["Trigger","utils",function(a,b){var c=function(){};return c.prototype.dayDisabled=function(){return!(this.triggers.temp1HighTimer_day.active||this.triggers.humidityHighTimer_day.active||this.triggers.cO2HighTimer_day.active||this.triggers.inactiveForTimer_day.active)},c.prototype.nightDisabled=function(){return!(this.triggers.temp1HighTimer_night.active||this.triggers.humidityHighTimer_night.active||this.triggers.cO2HighTimer_night.active||this.triggers.inactiveForTimer_night.active)},c.prototype.initTrigger=function(b,d){var e=a.unpack(b),f=e.triggerClass,g=["temp1HighTimer","humidityHighTimer","cO2HighTimer","inactiveForTimer"];if("Fan"==this.name&&-1!=g.indexOf(f)){if(e.since==this.day.since&&e.until==this.day.until)var h="day";else var h="night";f=e.triggerClass=e.triggerClass+"_"+h}if("timer"===f)this.intervals.push(e);else if("manualOn"===f)this.permOnTrigger=e,this.setPermOn(),this.permStatusSaved();else{var i=this.triggers[f];i?i.update(e):this.triggers[f]=e,e=this.triggers[f]}e.active=b.active!=c.FIRM_ACTIVITY_PERM_OFF,e.index=d},c.prototype.getReleasedIndexes=function(){var a=[];return this.getTriggersAndIntervals().forEach(function(b){b.isReleased()&&a.push(b.index)}),null!=this.permOnTrigger&&this.permOnTrigger.index>-1&&!this.isPermOn()&&a.push(this.permOnTrigger.index),a},c.prototype.prepareSave=function(){if("Fan"==this.name)for(var a in this.triggers)if(-1!=a.indexOf("_")){var b=this.triggers[a],d=b.triggerClass.split("_")[1],e=this[d];b.since=e.since,b.until=e.until}this.getTriggersAndIntervals().forEach(function(a){var b=this.isPermOn()?c.FIRM_ACTIVITY_PERM_OFF:this.getFirmwareActivityCode(a);a.prepareSave(b)},this),this.isPermOn()&&this.permOnTrigger.prepareSave(c.FIRM_ACTIVITY_PERM_ON)},c.prototype.useSlotIndex=function(a){var b=!1;return this.getTriggersAndIntervals().forEach(function(c){!b&&c.useSlotIndex(a)&&(b=!0)}),b?!0:this.isPermOn()?this.permOnTrigger.useSlotIndex(a):!1},c.prototype.saveTriggers=function(a){var b=[];this.getTriggersAndIntervals().forEach(function(a){a.index>-1&&!a.isReleased()&&b.push(a.index)}),this.isPermOn()&&b.push(this.permOnTrigger.index);var c=this;return async.series([function(a){async.forEachSeries(c.getTriggersAndIntervals(),function(a,b){a.saveTrigger(b)},function(b){a()})},function(a){c.isPermOn()?c.permOnTrigger.saveTrigger(a):a()},function(b){a(),b()}]),this.permStatusSaved(),b},c.prototype.deletePermOnTrigger=function(a){this.permOnTrigger.deleteTrigger(a),this.permOnTrigger=null},c.prototype.getTriggersAndIntervals=function(){var a=[];Array.prototype.push.apply(a,this.intervals);for(var b in this.triggers)a.push(this.triggers[b]);return a},c.prototype.getTrigger=function(b){if(null==this.triggers[b]){var c=b.split("_")[0],d=a.create(c);d.triggerClass=b,d.active=!1,d.output=this.outputIndex,this.triggers[b]=d}return this.triggers[b]},c.prototype.addInterval=function(){var b=a.create("timer");b.active=!0,b.output=this.outputIndex,this.intervals.push(b)},c.prototype.toggleInterval=function(a){var b=this.intervals[a];b.active=!b.active},c.prototype.permStatusSaved=function(){this.savedPermStatus=this.permStatus},c.prototype.permStatusSaveNeeded=function(){return this.savedPermStatus!=this.permStatus},c.prototype.setPermStatus=function(a){this.permStatus=a},c.prototype.setPermOn=function(){this.setPermStatus(c.PERM_ON),null==this.permOnTrigger&&(this.permOnTrigger=a.create("manualOn"),this.permOnTrigger.output=this.outputIndex,this.permOnTrigger.active=!0)},c.prototype.isPermOn=function(){return this.permStatus==c.PERM_ON},c.prototype.setPermOff=function(){this.setPermStatus(c.PERM_OFF)},c.prototype.isPermOff=function(){return this.permStatus==c.PERM_OFF},c.prototype.setPermAuto=function(){this.setPermStatus(c.AUTO)},c.prototype.isPermAuto=function(){return this.permStatus==c.AUTO},c.prototype.getFirmwareActivityCode=function(a){return this.isPermOn()?c.FIRM_ACTIVITY_PERM_ON:this.isPermOff()?c.FIRM_ACTIVITY_PERM_OFF:a.active?c.FIRM_ACTIVITY_AUTO:c.FIRM_ACTIVITY_PERM_OFF},$.extend(c,{PERM_ON:1,PERM_OFF:2,AUTO:3,FIRM_ACTIVITY_PERM_ON:2,FIRM_ACTIVITY_PERM_OFF:0,FIRM_ACTIVITY_AUTO:1,create:function(a,b){var d=new c;return d.name=a.name,d.partial=a.partial,d.outputIndex=b,d.permStatus=c.AUTO,d.savedPermStatus=c.AUTO,d.intervals=[],d.triggers={},d.permOnTrigger=null,"Fan"==d.name&&(d.day={since:null,until:null},d.night={since:null,until:null}),d}}),c}]),app.factory("Alert",["$http","utils","Relay",function(a,b,c){var d=function(){this.on_message="",this.off_message="",this.target=null,this.trigger=null};return d.prototype.pack=function(){return null===this.target&&""===this.on_message&&""===this.off_message?null:{on_message:this.on_message,off_message:this.off_message,target:this.target}},d.prototype.prepareSave=function(){var a=this.getFirmwareActivityCode();"powerDown"!==this.name?(this.trigger.off.val=this.trigger.on.val,this.trigger.prepareSave(a)):console.log("Power down.prepare save"),this.actualPack=this.pack(),this.actualPack&&(this.actualPack.active=a)},d.prototype.getReleasedIndexes=function(){return void 0!=this.index&&null==this.actualPack?this.index:-1},d.prototype.useSlotIndex=function(a,b){return null==this.actualPack?!1:(console.log("Use slot index: "+a+" "+b+", this.index = "+this.index),console.log(this.actualPack),void 0==this.index?(this.index=a,this.trigger?(this.trigger.useSlotIndex(b),this.actualPack.trigger=b):this.actualPack.trigger=-2,console.log(this.actualPack),!0):(this.trigger?this.actualPack.trigger=this.trigger.index:this.actualPack.trigger=-2,console.log(this.actualPack),!1))},d.prototype.save=function(c,d){var e=this;this.index>-1?(d.push(this.index),b.deepCompare(this.actualPack,this.origin)?e.trigger?e.trigger.saveTrigger(c):c():async.series([function(b){e.origin=e.actualPack,a.post("/alerts/"+e.index+".jso",e.actualPack).success(function(a){b()})},function(a){e.trigger?e.trigger.saveTrigger(c):(a(),c())}],function(a){})):c()},d.prototype.getFirmwareActivityCode=function(){return this.active?c.FIRM_ACTIVITY_AUTO:c.FIRM_ACTIVITY_PERM_OFF},$.extend(d,{createDisabled:function(a){return{on_message:"",off_message:"",target:"",trigger:-1,index:a}},loadMany:function(b,c,d){if(0===b.length)return void d();var e=async.queue(function(b,d){a.get("/alerts/"+b+".jso",{cache:!1}).success(function(a){console.log("alert #"+b+" loaded",a),a.index=b,c(a)})["finally"](d)},1);e.drain=function(){$.isFunction(d)&&d()},b.forEach(function(a){e.push(a)})},loadRaw:function(b,c,d){a.get("/alerts/"+b+".jso",{cache:!1}).success(function(b){if(b.trigger>-1){a.get("/triggers/"+b.trigger+".jso",{cache:!1}).success(function(a){b.triggerData=a,c(b),d()})}else c(b),d()})},save:function(b,c){if(!b.length)return void c();var d=async.queue(function(b,c){var d=b.index;a.post("/alerts/"+d+".jso",b)["finally"](c)},1);d.drain=function(){c()},b.forEach(function(a){d.push(a)})},saveInactive:function(b,c){if(!b.length)return void c();var d=async.queue(function(b,c){var d=b.index;console.log("Inactive alert #"+d+" saved",b),a.post("/alerts/alert"+d+".jso",b)["finally"](c)},1);d.drain=function(){c()},b.forEach(function(a){d.push(a)})}}),d}]),app.controller("NavigationController",["$scope","$location",function(a,b){a.isActive=function(a){return a===b.path()}}]),app.controller("CalibrationController",["$scope","$http","$timeout","CalibrationConfig",function(a,b,c,e){function f(){a.twoPointCalibrationData=[];for(var b=0;b<calibrationDataConfig.length;b++)d=calibrationDataConfig[b],cd=g(d),a.twoPointCalibrationData.push(cd)}function g(a){return data={},data.sensorId=a,data.records=[],data.records.push(h(10*a.id,a.divisor)),data.records.push(h(10*a.id+1,a.divisor)),data}function h(b,c){return recordData=a.config.calibration_data[""+b],record={},record.reading=void 0==recordData?"":recordData.sensor_reading/c,record.realVal=void 0==recordData?"":recordData.real_value/c,record}function i(b){recordIndex=10*a.popup_data.sensorId.id+b,divisor=a.popup_data.sensorId.divisor,record=a.popup_data.records[b],realVal=parseFloat(record.realVal),reading=parseFloat(record.reading),isNaN(realVal)?(console.log(a.config.calibration_data),delete a.config.calibration_data[recordIndex],console.log(a.config.calibration_data)):isNaN(reading)?(alert("Data error: Cannot save calibration pair with no Sensor data, index: '"+b+"'"),console.log(record)):a.config.calibration_data[""+recordIndex]={id:recordIndex,sensor_reading:reading*divisor,real_value:realVal*divisor,sensor:a.popup_data.sensorId.id}}function j(a,b){return"Light-Out"!=a&&"Light-In"!=a&&"DHT22-Temp"!=a&&"DHT22-Hum"!=a&&"Temp-Water"!=a&&"Temp-Bulb"!=a&&"USND"!=a||-999!=b?"EC"==a&&(1>b||b>1e3)?!1:"pH"==a&&(1>b||b>1e3)?!1:!0:!1}function k(b,c,d){return a.loadingStep+=d,a.loadingPercent=parseInt(a.loadingStep/a.stepCount*100,10),curStep++,curRetry=0,console.log("rawDataSuccess: ",b,c,c+b,curStep),c+b}function l(){a.loadingStep+=1,a.loadingPercent=parseInt(a.loadingStep/a.stepCount*100,10),curRetry++}function m(b){console.log("Calibration done",curStep,total),curStep==calibrationNumSteps?(a.config[b]=""+Math.round(total/calibrationNumSteps),a.needsSavingArray[b]=!0,a.lastCalibrationFailedArray[b]=!1):(a.needsSavingArray[b]=!1,a.lastCalibrationFailedArray[b]=!0)}function n(b,c){if(curStep==calibrationNumSteps){var d=total/calibrationNumSteps;a.popup_data.records[b].reading=+(d/c).toFixed(1)}else alert("Error occured while reading sensor data; Sensor is disconnected or has a malfunction")}a.loading=!0,a.saving=!1,a.calibratingArray=[],a.needsSavingArray=[],a.lastCalibrationFailedArray=[],a.twoPointCalibrationData=[],calibrationNumSteps=5,calibrationMeasurementDelay=2e3,calibrationNumRetries=1,ECMaxAcceptableValue=1500,a.loadCalibrationConfig=function(){e.config.get().then(function(b){a.config=b,f(),a.loading=!1},function(b){alert("Fail: "+b),a.loading=!1,a.config={}})},a.loadCalibrationConfig(),calibrationDataConfig=[{id:2,name:"Light-Out",divisor:10},{id:5,name:"Light-In",divisor:10},{id:1,name:"DHT22-Temp",divisor:10},{id:0,name:"DHT22-Hum",divisor:10},{id:4,name:"Temp-Water",divisor:10},{id:6,name:"Temp-Bulb",divisor:10},{id:3,name:"USND",divisor:1},{id:9,name:"CO2",divisor:1}],a.open_popup_window=function(b){a.popup_data=g(b),a.show_calibration_popup_window=!0},a.close_popup_window=function(){a.show_calibration_popup_window=!1},a.sendCalibrationData=function(){i(0),i(1),console.log(a.config.calibration_data),a.close_popup_window(),a.loading=!0,a.save(a.loadCalibrationConfig)},a.getRawData=function(b,c,d,f,g){a.loadingMessage="Calibrating "+c,a.loading=!0,a.stepCount=(calibrationNumRetries+1)*calibrationNumSteps,a.loadingStep=0,a.loadingPercent=0,a.calibrating=!0,a.calibratingArray[b]=!0,curStep=0,curDelay=0,curRetry=0,total=0,async.whilst(function(){return curStep<calibrationNumSteps&&curRetry<=calibrationNumRetries},function(a){setTimeout(function(){console.log("Trying to read senzor '"+c+"'"),e[c].get(function(b){rawValue=parseFloat(b.raw_value),j(c,rawValue)?total=k(rawValue,total,calibrationNumRetries+1-curRetry):l(),a()},function(){l(),a()})},curDelay)},function(){d?n(f,g):m(b),a.loading=!1,a.calibrating=!1,a.calibratingArray[b]=!1})},a.save=function(d){a.saving||(a.saving=!0,b.post("calib.jso",a.config).success(function(){a.saving=!1,a.saveSuccess=!0,a.needsSavingArray=[],d?(a.saveSuccess=!1,d()):c(function(){a.saveSuccess=!1},2e3)},function(){alert("Oops save failed.")}))}}]),app.controller("ChartController",["$scope","$rootScope","$location","utils","SensorHistory","settings","requests","$interval","RelayData","$http",function(a,b,c,d,e,f,g,h,i,j){function k(){Highcharts.setOptions({global:{useUTC:!1}});var b=0,c=Highcharts.getOptions().colors;x.forEach(function(e,f){var g=d.generateId();$('<div class="chart"></div>').attr("id",g).appendTo("#charts");var h={chart:{renderTo:g,type:"spline"},title:{text:"Sensors",style:{display:"none"}},xAxis:{type:"datetime",maxZoom:3e5,title:{text:"Time"}},yAxis:e.yAxis,series:[]};window.isTouch||(h.chart.zoomType="x"),w[f]=new Highcharts.Chart(h),e.series.forEach(function(d){var e={name:d.name,color:c[b++],yAxis:d.yAxis};window.isTouch||(e.events={click:function(b){"H"!==a.zoom&&p("M"===a.zoom?"D":"H",moment(b.point.x))}}),w[f].addSeries(e)})}),l()}function l(){g.clear(),w.forEach(function(a){a.series.forEach(function(a){a.hide()})})}function m(b){return"H"==a.zoom?d.arrayPad(b,60,null):"D"==a.zoom?d.arrayPad(b,24,null):"M"==a.zoom?d.arrayPad(b,d.daysInMonth(a.dt.month(),a.dt.year()),null):void 0}function n(a,b){a.startOf(y[b].momentUnit)}function o(b,c,d,f){l(),w.forEach(function(b){b.showLoading("Loading…"),b.numErrorSeries=0,b.series.forEach(function(b){b.setData([]),b.update($.extend({cursor:"H"===a.zoom?"default":"pointer"},f))})}),e[c](d,function(a,c){var d=null,e=null;a:for(var f=0;f<w.length;f++)for(var g=0;g<x[f].series.length;g++)if(x[f].series[g].resource===a){d=w[f],e=d.series[g];break a}null===e?console.warn("No series for "+a):(c&&e.setData(m(c[b])),d.hideLoading(),e.show())},function(a){var b=null,c=null;a:for(var d=0;d<w.length;d++)for(var e=0;e<x[d].series.length;e++)if(x[d].series[e].resource===a){b=w[d],c=b.series[e];break a}console.log("Error downloading data for sensor "+a),b.numErrorSeries++,b.numErrorSeries==b.series.length&&b.showLoading("No data available")})}function p(d,e){var f=a.zoom!==d,g="now"===e;g?(e=moment(),a.isCurrent=!0,"H"===d&&e.subtract("hour",1).startOf("minute")):a.isCurrent=!1,g&&console.log("now!!!"),console.log("dt: ",e.format("YYYY/MM/DD hh:mm:ss"),", zone: ",e.zone()),g&&"H"===d||n(e,d),b.$emit("displayedTimeChanged",moment(e),d,g),a.dt=e,a.zoom=d,zt=y[d],c.search("z",d),c.search("d",g&&"H"===d?null:e.format(zt.urlFormat));var h={},i=g?moment():e;for(var j in zt.dateComponents)h[j]=i.format(zt.dateComponents[j]);var k={pointStart:e.valueOf(),pointInterval:zt.pointInterval};g&&"H"===d?o(zt.dataKey,"get",{},k):o(zt.dataKey,zt.resourceMethod,h,k),f&&r(),q();var l=y[a.zoom].momentFormat;a.formattedDate=$.isFunction(l)?l(a.dt):a.dt.format(l),a.forwardDisallowed=a.isCurrent||t(a.dt,a.zoom,1).unix()>moment().unix()}function q(){var b=$("#picker-date").data("datetimepicker");b&&b.setDate(a.dt.toDate())}function r(){zt=y[a.zoom],$("#picker-date").datetimepicker("remove"),$("#picker-date").datetimepicker({minView:3,maxView:zt.pickerMaxView,startView:zt.pickerMaxView,format:zt.pickerFormat,language:"cs",startDate:new Date(2013,0,1),endDate:new Date}).on("changeDate",function(b){var c=moment(b.date).add("minutes",b.date.getTimezoneOffset());p(a.zoom,c),$("#picker-date").datetimepicker("hide")}),q()}function s(b){p(b,a.dt)}function t(a,b,c){var d=moment(a);return d.add(y[b].momentUnit+"s",c),d}function u(b){var c;a.isCurrent&&"H"==a.zoom?(c=moment(a.dt),n(c,a.zoom)):c=t(a.dt,a.zoom,b),c.unix()<=moment().unix()&&(a.isCurrent=!1,p(a.zoom,c))}function v(){p(a.zoom,"now")}var w=[],x=f.charts,y={H:{dataKey:"min",resourceMethod:"getHour",dateComponents:{year:"YYYY",month:"MM",day:"DD",hour:"HH"},pointInterval:6e4,pickerMaxView:1,pickerFormat:"d. MM yyyy hh:ii",momentFormat:function(a){return a.format("D. MMMM YYYY HH:mm")+" - "+moment(a).add("h",1).format("HH:mm")},momentUnit:"hour",urlFormat:"YYYY-MM-DDTHH:mm"},D:{dataKey:"h",resourceMethod:"getDay",dateComponents:{year:"YYYY",month:"MM",day:"DD"},pointInterval:36e5,pickerMaxView:2,pickerFormat:"d. MM yyyy",momentFormat:"D. MMMM YYYY",momentUnit:"day",urlFormat:"YYYY-MM-DD"},M:{dataKey:"day",resourceMethod:"getMonth",dateComponents:{year:"YYYY",month:"MM"},pointInterval:864e5,pickerMaxView:3,pickerFormat:"MM yyyy",momentFormat:"MMMM YYYY",momentUnit:"month",urlFormat:"YYYY-MM-DD"}};a.changeZoom=s,a.shiftDateUnit=u,a.showRecent=v,k();var z=c.search();a.zoom=y[z.z]?z.z:"H",p(a.zoom,z.d?moment(z.d):"now"),r(),q()}]),app.controller("SettingsController",["$http","$scope","$timeout","$interval","BackendConfig",function(a,b,c,d,e){b.loading=!0,b.saving=!1,b.wifi_connected=!1,b.$on("$destroy",function(){d.cancel(b.wifi_status_updater)}),e.get(function(a){a.use_dhcp=!!a.use_dhcp,b.config=a,b.loading=!1,b.known_wifis=[],b.selected_index_in_modal=-1,b.model={},b.model.entered_password=""}),b.save=function(){b.saving||(b.saving=!0,b.config.$save().then(function(){b.saving=!1,b.saveSuccess=!0,c(function(){b.saveSuccess=!1},2e3)},function(){alert("Oops save failed.")}))},b.wifi_status_updater=d(function(){a.get("/wifi_active.jso",{cache:!1}).success(function(a){b.wifi_connected=null!=a.ssid})},1e3),b.scan_for_wifis=function(){b.show_wifi_window=!0,a.get("/wifilist.jso",{cache:!1}).success(function(a){angular.copy(a.networks,b.known_wifis)})},b.close_wifi_window=function(){b.show_wifi_window=!1},b.select_wifi_in_modal=function(a,c){a.encrypted||b.selected_index_in_modal!=c?(b.model.entered_password="",b.selected_index_in_modal=c):(b.selected_index_in_modal=-1,b.select_wifi_in_controller(a))},b.select_wifi_in_controller=function(c){b.config.wifi_ssid=c.ssid,b.config.wifi_pwd=b.model.entered_password,b.close_wifi_window();var d={SSID:c.ssid,password:b.model.entered_password};a.post("/partial/config.jso",d).success(function(a){console.log("Connect data succesfully POSTed to /partial/config.jso")})},b.test_email_window=function(){b.show_test_email_window=!0},b.close_test_email_window=function(){b.test_email_error_msg="",b.show_test_email_window=!1},b.send_test_email=function(){var c="send_test_mail?",d="smtp="+b.config.smtp,e="&smtp_port="+b.config.smtp_port,f="&smtp_ssl="+b.config.smtp_ssl,g="&smtp_user="+b.config.smtp_user,h="&smtp_pwd="+b.config.smtp_pwd,i="&mail_from="+b.config.mail_from,j="&to="+b.config.test_email,k=c+d+e+f+g+h+i+j;a.get(k,{cache:!1}).success(function(a){alert("Response received"),a.success?(b.test_email_error_msg="",b.close_test_email_window()):b.test_email_error_msg=a.code})}}]),app.controller("RelayDataController",["$scope","$interval","settings","RelayData","OutputsData","utils","$rootScope","$http",function(a,b,c,d,e,f,g,h){function i(a){if(a>2147483647||-2147483648>a)throw new TypeError("arrayFromMask - out of range");for(var b=a,c=[];b;c.push(Boolean(1&b)),b>>>=1);return c}function j(b){console.log("parseOutputsData",b);var d=i(b.end_state);a.relays=[],c.outputs.forEach(function(b,c){a.relays.push({name:b.name,state:d.length>c?d[c]:!1})});for(var e=[],f=0;f<b.history.length;f++){for(var g=b.history[f].state,h=[],j=0;j<g.length;j++){var k=parseInt(g[j].output),l=c.outputs[k]?c.outputs[k].name:""+k;h.push({name:l,on:g[j].state})}var m=b.history[f].when,n=moment(m).startOf("day"),o=e[e.length-1],p={when:m,relays:h,show:b.history[f].show};b.history[f].show||console.log("not showing item for ",m.format("HH:mm")),o&&o.when.isSame(n)?(console.log("no push"),o.items.push(p)):(console.log("push"),e.push({when:n,items:[p]}))}a.history=e,a.loadingHistory=!1}function k(b){console.log("fillCurrentRelaysData",b);var d=i(b.currentState);a.relays=[],c.outputs.forEach(function(b,c){a.relays.push({name:b.name,state:d.length>c?d[c]:!1})})}function l(){g.reloadTimer&&(clearInterval(g.reloadTimer),delete g.reloadTimer)}function m(){g.reloadTimer||(g.reloadTimer=setInterval(u,3e4))}function n(a,b){var c=a.diff(b,"minutes");return a.isBefore(b)||c>60?(console.log("Filtering out datum for "+a.format("YYYY/MM/DD HH:mm,")+"diff: "+c+", daytime: "+b.format("YYYY/MM/DD HH:mm")),!1):(console.log("Keeping datum for "+a.format("YYYY/MM/DD HH:mm,")+"diff: "+c+", daytime: "+b.format("YYYY/MM/DD HH:mm")),!0)}function o(a,b,c,d){(!c||n(a,c))&&historyData.history.push({when:a,state:b,show:d})}function p(a,b){var c=void 0;for(var d in a.state){var e=moment.unix(d);(void 0==c||c>d)&&(c=d),o(e,a.state[d],b,!0)}o(moment.unix(c).subtract({seconds:1}),a.initial,null,!1)}function q(a,b,c,d){var e=a+b+".jso";h.get(e,{headers:{"Cache-Control":"no-cache",Pragma:"no-cache"},cache:!1}).then(function(e){0==b&&c&&historyData.history.push({when:moment(c).startOf("day"),state:e.data.initial?e.data.initial:0,show:!1}),p(e.data,c),async.series([function(d){q(a,b+1,c,d)},function(a){d(),a()}])},function(a){d()})}function r(a,b){historyData={},historyData.history=[],async.series([function(c){q(a,0,b,c)},function(a){historyData.history.sort(function(a,b){return b.when.valueOf()-a.when.valueOf()}),k(historyData),a()}])}function s(a,b){historyData={},historyData.history=[],h.get(a,{headers:{"Cache-Control":"no-cache",Pragma:"no-cache"},cache:!1}).then(function(a){console.log("Loaded data"),console.log(a),historyData.end_state=a.data.end_state;for(var c in a.data.state){var d=moment.unix(c);!b||n(d,b)?historyData.history.push({when:d,state:a.data.state[c],show:!0}):(console.log("ignored"),console.log(d),console.log(a.data.state[c]))}historyData.history.sort(function(a,b){return b.when.valueOf()-a.when.valueOf()}),j(historyData)},function(b){console.log("Data from '"+a+"' could not be read: '"+b+"'")})}function t(a,b){if(useOldHistoryDataLoadingMethodUsingOutputsInsteadOfOutputChanges){
var c="/DATA/OUTPUTS/"+a;r(c,b)}else{var d="/DATA/OUTPUT_CHANGES/"+a+"0.jso";s(d,b)}}function u(b,c,d){if(b&&console.log("refreshRelays daytime: ",b.format("YYYY/MM/DD HH:mm")," zone: ",b.zone()),a.loadingHistory=!0,a.showMonthMessage=!1,a.history={},l(),!c||d&&"H"==c)s("/sensors/output_changes.jso"),m();else if("H"==c){var e=b.format("YYYY/MM/DD/");t(e,b)}else if("D"==c){var e=b.format("YYYY/MM/DD/");t(e)}else a.showMonthMessage=!0,a.loadingHistory=!1}useOldHistoryDataLoadingMethodUsingOutputsInsteadOfOutputChanges=!1,historyData={},g.$on("displayedTimeChanged",function(a,b,c,d){u(b,c,d)})}]),app.controller("RelayController",["$scope",function(a){}]),app.controller("TriggerController",["$scope",function(a){a.init=function(b,c){a.t=b.getTrigger(c)},a.toggle=function(){a.t.active=!a.t.active}}]),app.controller("TriggersController",["$scope","$http","$timeout","utils","Relay","Trigger","ClientConfig","settings",function(a,b,c,d,e,f,g,h){function i(){var a=j.indexOf(-1);if(-1!=a)return a;throw alert("Too many triggers!"),"Too many triggers!"}a.relays=[],a.relaysHash={},a.showInvalids=!1,h.outputs.forEach(function(b,c){var d=e.create(b,c);a.relays.push(d),a.relaysHash[b.name]=d});var j=d.newArray(f.LENGTH,-1),k={};a.loadingMessage="Loading triggers",a.loading=!0,a.loadingStep=0,a.loadingPercent=0,g.get().then(function(b){if(k=b,a.stepCount=k.usedTriggers?k.usedTriggers.length:0,(k.permOffRelays||[]).forEach(function(b){a.relaysHash[b].setPermOff(),a.relaysHash[b].permStatusSaved()}),null!=k.fanTimesInfo){var c=a.relaysHash.Fan;c.day.since=-1==k.fanTimesInfo[0]?"00:00":d.minutesToTime(k.fanTimesInfo[0]),c.day.until=-1==k.fanTimesInfo[0]?"00:00":d.minutesToTime(k.fanTimesInfo[1]),c.night.since=-1==k.fanTimesInfo[2]?"00:00":d.minutesToTime(k.fanTimesInfo[2]),c.night.until=-1==k.fanTimesInfo[2]?"00:00":d.minutesToTime(k.fanTimesInfo[3])}async.forEachSeries(k.usedTriggers||[],function(b,c){f.loadRaw(b,function(c){var d=a.relays[c.output];d?(d.initTrigger(c,b),j[b]=d.outputIndex):console.warn("Loaded trigger for undefined output "+c),a.loadingStep+=1,a.loadingPercent=parseInt(a.loadingStep/a.stepCount*100,10)},c)},function(b){a.loading=!1})}),a.saveTriggers=function(){var e=document.getElementsByClassName("ng-invalid");if(0!=e.length){var f=e[1];return f.scrollIntoView(!0),f.focus(),void(a.showInvalids=!0)}a.saving=!0,a.relays.forEach(function(a){a.prepareSave()}),a.relays.forEach(function(a){a.getReleasedIndexes().forEach(function(a){j[a]=-1})}),a.relays.forEach(function(a){do{var b=i(),c=a.useSlotIndex(b);c&&(j[b]=a.outputIndex)}while(c)});var g=[];a.relays.forEach(function(a){a.isPermOff()&&g.push(a.name)});var h=a.relaysHash.Fan,l=[null===h.day.since?-1:d.timeToMinutes(h.day.since),null===h.day.since?0:d.timeToMinutes(h.day.until),null===h.night.since?-1:d.timeToMinutes(h.night.since),null===h.night.since?0:d.timeToMinutes(h.night.until)],m=[];async.series([function(b){async.forEachSeries(a.relays,function(a,b){Array.prototype.push.apply(m,a.saveTriggers(b))},function(a){b()})},function(b){async.forEachSeries(a.relays,function(a,b){if(null!=a.permOnTrigger){var c=a.permOnTrigger.index;c>-1&&-1==j[c]?a.deletePermOnTrigger(b):b()}else b()},function(a){b()})},function(a){d.deepCompare(g,k.permOffRelays)&&d.deepCompare(m,k.usedTriggers)&&d.deepCompare(l,k.fanTimesInfo)?a():(k.permOffRelays=g,k.usedTriggers=m,k.fanTimesInfo=l,b.post("client.jso",k).success(function(){a()}))},function(b){a.saving=!1,a.saveSuccess=!0,c(function(){a.saveSuccess=!1},2e3),b()}])}}]),app.controller("AlertController",["$scope",function(a){a.init=function(b){a.alert=a.$parent.getAlert(b)},a.toggle=function(){a.alert.active=!a.alert.active}}]),app.controller("AlertsController",["$http","$scope","$timeout","utils","Alert","Trigger","ClientConfig",function(a,b,c,d,e,f,g){function h(a,c,d){var e=b.alerts[c];return e.target=a.target,e.on_message=a.on_message,e.off_message=a.off_message,e.active=1==a.active,e.origin=a,e.index=d,l[d]=e,e}function j(){var a=l.indexOf(-1);return-1!=a?a:void 0}function k(){var a=m.indexOf(-1);return-1!=a?a+o:void 0}b.getAlert=function(a){if(null==b.alerts[a]){var c=new e;if(c.active=!1,c.name=a,"powerDown"!==a){var d=f.create(a);d.output=-1,c.trigger=d}b.alerts[a]=c}return b.alerts[a]},b.loadingMessage="Loading alerts",b.loading=!0,b.loadingStep=0,b.loadingPercent=0,b.alerts={};var l=d.newArray(settings.alertLimit,-1),m=d.newArray(settings.alertLimit,-1),n={},o=settings.triggerCount-settings.alertLimit;g.get().then(function(a){n=a,b.stepCount=n.usedAlerts?n.usedAlerts.length:0,console.log("Alerts to read:"),console.log(n.usedAlerts),async.forEachSeries(n.usedAlerts||[],function(a,c){e.loadRaw(a,function(c){if(console.log("Read alert data:"),console.log(c),console.log(c.triggerData),c.triggerData){c.triggerData.index=c.trigger;var d=f.unpack(c.triggerData);m[c.trigger-o]=d,console.log("TriggerSlots:"),console.log(m),delete c.triggerData;var e=h(c,d.triggerClass,a);e.trigger=d}else var e=h(c,"powerDown",a);b.loadingStep+=1,b.loadingPercent=parseInt(b.loadingStep/b.stepCount*100,10)},c)},function(a){b.loading=!1})}),b.saveAlerts=function(){var e=document.getElementsByClassName("ng-invalid");if(0!=e.length){var f=e[1];return f.scrollIntoView(!0),f.focus(),void(b.showInvalids=!0)}b.saving=!0,$.each(b.alerts,function(a,b){b.prepareSave()}),$.each(b.alerts,function(a,b){var c=b.getReleasedIndexes();c>-1&&(console.log("Index was released: "+c),l[releadesIndex]=-1)}),$.each(b.alerts,function(a,b){var c=j(),d=k(),e=b.useSlotIndex(c,d);e&&(l[c]=b,b.trigger&&(m[d-o]=b.trigger))}),console.log("slots:"),console.log(l);var g="";for(i=0;i<l.length;i++)g+=l[i]&&l[i].trigger?l[i].trigger.index+" ":"- ";console.log(g);var h=[],p=[];$.each(b.alerts,function(a,b){p.push(b)}),async.series([function(a){async.forEachSeries(p,function(a,b){a.save(b,h)},function(b){a()})},function(b){console.log("usedAlerts:"),console.log(h),d.deepCompare(h,n.usedAlerts)?b():(n.usedAlerts=h,a.post("client.jso",n).success(function(){b()}))},function(a){b.saving=!1,b.saveSuccess=!0,c(function(){b.saveSuccess=!1},2e3),a()}])}}]),app.controller("MaintenanceController",["$http","$scope","Alert","Trigger","BackendConfig","ClientConfig","CalibrationConfig",function(a,b,c,d,e,f,g){settingsFileSelector=document.getElementById("settingsFileSelector"),calibrationFileSelector=document.getElementById("calibrationFileSelector"),ouputsAlertsFileSelector=document.getElementById("outputsAlertsFileSelector"),b.initSaveClientJSO=function(){var a={fileName:"config.jso",file:""};b.formData=a,b.$parent.form1Data=a},b.saveClientJSO=function(){setupLoadingBarWithMessage("Loading"),b.$parent.loading=!0,e.get().then(function(a){a.$promise=void 0,a.$resolved=void 0,save2File(JSON.stringify(a),b.form1Data.fileName),b.$parent.loading=!1})},b.loadConfig=function(){settingsFileSelector.click()},b.loadClientJSO=function(a){loadFile(a,uploadClientJSO),settingsFileSelector.value=null},uploadClientJSO=function(c){setupLoadingBarWithMessage("Saving"),b.$parent.loading=!0,a.post("config.jso",c).success(function(){b.$parent.loading=!1})},b.initSaveCalibJSO=function(){var a={fileName:"calib.jso",file:""};b.formData=a,b.$parent.form6Data=a},b.saveCalibJSO=function(){setupLoadingBarWithMessage("Loading"),b.$parent.loading=!0,g.config.get().then(function(a){a.$promise=void 0,a.$resolved=void 0,save2File(JSON.stringify(a),b.form6Data.fileName),b.$parent.loading=!1})},b.loadCalib=function(){calibrationFileSelector.click()},b.loadCalibJSO=function(a){loadFile(a,uploadCalibJSO),calibrationFileSelector.value=null},uploadCalibJSO=function(c){setupLoadingBarWithMessage("Saving"),b.$parent.loading=!0,a.post("calib.jso",c).success(function(){b.$parent.loading=!1})},b.initBackupTriggers=function(){var a={fileName:"backup.txt",file:""};b.formData=a,b.$parent.form2Data=a},b.backupTriggers=function(){setupLoadingBarWithMessage("Loading"),b.$parent.loading=!0,f.get().then(function(a){b.$parent.stepCount=a.usedAlerts?a.usedAlerts.length:0,b.$parent.stepCount+=a.usedTriggers?a.usedTriggers.length:0,a.$promise=void 0,a.$resolved=void 0;var e={client:a};async.forEachSeries(a.usedAlerts||[],function(a,d){c.loadRaw(a,function(c){c.trigger>-1&&(e["/triggers/"+c.trigger]=c.triggerData,delete c.triggerData),e["/alerts/"+a]=c,b.$parent.loadingStep+=1,b.$parent.loadingPercent=parseInt(b.loadingStep/b.stepCount*100,10)},d)},function(c){async.forEachSeries(a.usedTriggers||[],function(a,c){d.loadRaw(a,function(c){e["/triggers/"+a]=c,b.$parent.loadingStep+=1,b.$parent.loadingPercent=parseInt(b.loadingStep/b.stepCount*100,10)},c)},function(a){save2File(JSON.stringify(e),b.form2Data.fileName),b.$parent.loading=!1})})})},b.loadAlertsTriggers=function(){ouputsAlertsFileSelector.click()},b.loadTriggers=function(a){loadFile(a,uploadTriggers),ouputsAlertsFileSelector.value=null},uploadTriggers=function(c){setupLoadingBarWithMessage("Saving"),b.$parent.loading=!0,c=JSON.parse(c),b.$parent.stepCount=Object.keys(c).length,async.forEachSeries(Object.keys(c)||[],function(d,e){a.post(d+".jso",c[d]).success(function(){b.$parent.loadingStep+=1,b.$parent.loadingPercent=parseInt(b.loadingStep/b.stepCount*100,10),e()})},function(a){b.$parent.loading=!1})},b.deleteAlertsTriggers=function(){setupLoadingBarWithMessage("Deleting"),b.$parent.loading=!0,f.get().then(function(d){void 0==d.usedTriggers&&(d.usedTriggers=[]),b.$parent.stepCount=d.usedAlerts?3*d.usedAlerts.length:0,b.$parent.stepCount+=d.usedTriggers?d.usedTriggers.length:0,async.forEachSeries(d.usedAlerts||[],function(a,b){c.loadRaw(a,function(a){a.trigger>-1&&d.usedTriggers.push(a.trigger)},b)},function(c){async.forEachSeries(d.usedAlerts||[],function(c,d){a.post("/alerts/"+c+".jso","").success(function(){b.$parent.loadingStep+=1,b.$parent.loadingPercent=parseInt(b.loadingStep/b.stepCount*100,10),d()})},function(c){async.forEachSeries(d.usedTriggers||[],function(c,d){a.post("/triggers/"+c+".jso","").success(function(){b.$parent.loadingStep+=1,b.$parent.loadingPercent=parseInt(b.loadingStep/b.stepCount*100,10),d()})},function(c){a.post("client.jso","").success(function(){b.$parent.loading=!1})})})})})},setupLoadingBarWithMessage=function(a){b.$parent.loadingMessage=a,b.$parent.loadingStep=0,b.$parent.loadingPercent=0},save2File=function(a,b){var c=new Blob([a],{type:"text/plain"}),d=document.getElementById("a");d.href=URL.createObjectURL(c),d.download=b,d.click()},loadFile=function(a,b){var c=a.files[0],d=new FileReader;d.onload=function(a){b(a.target.result)},d.readAsText(c)}}]),app.controller("ClientUploadController",["$scope",function(a){}]),app.controller("GrowRoomController",["$scope",function(a){}]),app.directive("bsHasError",[function(){return{restrict:"A",link:function(a,b,c,d){var e=b.find("input[ng-model]");e&&a.$watch(function(){return e.hasClass("ng-invalid")},function(a){b.toggleClass("has-error",a)})}}}]);